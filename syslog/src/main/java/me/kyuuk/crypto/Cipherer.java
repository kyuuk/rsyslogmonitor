package me.kyuuk.crypto;

import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.DecoderException;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Cipherer
{
	/**
	 * Algorithme XTEA
	 */
	public static final String XTEA_ALGO = "XTEA";

	private static final int EXPECTED_KEY_SIZE = 128 / 8;

	private final SecretKey secretKey;

	public Cipherer(final String keyAsString)
	{
		CryptoCommons.addBouncyCastleProvider();

		final MessageDigest shaDigester = new MessageDigest();

		final byte[] keyAsBytes = shaDigester.digest(keyAsString);
		this.secretKey = new SecretKeySpec(getKey(keyAsBytes), "XTEA");
	}

	private byte[] cipher(final String content) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException
	{
		final Cipher cipher = Cipher.getInstance("XTEA", BouncyCastleProvider.PROVIDER_NAME);
		cipher.init(Cipher.ENCRYPT_MODE, this.secretKey);
		return cipher.doFinal(content.getBytes());
	}

	public String cipherString(final String data) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException
	{
		final byte[] cipheredContent = cipher(data);
		final byte[] encodedContent = Base64.encode(cipheredContent);
		return new String(encodedContent, StandardCharsets.UTF_8);
	}

	public static byte[] getKey(final byte[] key)
	{
		if (key.length < EXPECTED_KEY_SIZE)
		{
			throw new InvalidParameterException(
					"Key length is invalid: " + key.length + ". Expected a key length of " + EXPECTED_KEY_SIZE);
		} else if (key.length > EXPECTED_KEY_SIZE)
		{
			log.debug("Taille de cle trop grande [{}] on reduit la taille a [{}]", key.length, EXPECTED_KEY_SIZE);
			final byte[] tmp = new byte[EXPECTED_KEY_SIZE];
			System.arraycopy(key, 0, tmp, 0, EXPECTED_KEY_SIZE);
			log.debug("Nouvelle taille de cle [{}]", tmp.length);
			return tmp;
		}
		return key;
	}

	/**
	 * Dechiffre une chaine de caractere
	 *
	 * @param ciphered la chaine à dechiffrer
	 * @return retourne la chaine déchiffrée ou null en cas d'erreur
	 */
	public String decipherString(final String ciphered)
	{
		if (ciphered == null)
		{
			return "";
		}
		final byte[] encodedBytes = ciphered.getBytes(StandardCharsets.UTF_8);
		final byte[] cipheredContent = Base64.decode(encodedBytes);

		return decipher(cipheredContent);
	}

	private String decipher(final byte[] cipheredContent)
	{
		try
		{
			final Cipher cipher = Cipher.getInstance(XTEA_ALGO);
			cipher.init(Cipher.DECRYPT_MODE, this.secretKey);
			final byte[] deciphered = cipher.doFinal(cipheredContent);
			return new String(deciphered, StandardCharsets.UTF_8);
		} catch (final GeneralSecurityException e)
		{
			log.error("Erreur de dechiffrement : ", e);
			return null;
		}
	}

	public boolean isCiphered(final String content)
	{
		try
		{
			final byte[] encodedBytes = content.getBytes(StandardCharsets.UTF_8);
			final byte[] cipheredContent = Base64.decode(encodedBytes);
			final Cipher cipher = Cipher.getInstance(XTEA_ALGO);
			cipher.init(Cipher.DECRYPT_MODE, this.secretKey);
			cipher.doFinal(cipheredContent);
			return true;
		} catch (GeneralSecurityException | DecoderException e)
		{
			return false;
		}
	}
}
