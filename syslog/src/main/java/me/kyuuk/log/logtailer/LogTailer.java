package me.kyuuk.log.logtailer;

import java.io.File;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.lang3.StringUtils;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class LogTailer
{
	private static final int DEFAULT_DELAY = 5000;
	private static final StringBuilder logs = new StringBuilder();

	private LogTailer()
	{
	}

	public static void createLogTailer(final String fileName)
	{
		if (StringUtils.isBlank(fileName))
		{
			log.warn("No log File configured, cannot show application logs in web UI");
			return;
		}
		log.info("creating log tailer for file [{}]", fileName);
		final Tailer tailer = createTailer(fileName);
		final Thread tailerThread = new Thread(tailer);
		tailerThread.setDaemon(true);
		addShutdownHook(tailer);
		tailerThread.start();
	}

	private static void addShutdownHook(final Tailer tailer)
	{
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			@Override
			public void run()
			{
				log.debug("Shutting down tailer thread for file [{}]", tailer.getFile().getAbsolutePath());
				tailer.stop();
			}
		});
	}

	private static Tailer createTailer(final String fileName)
	{

		final LogTailerListener listener = new LogTailerListener();
		final File file = new File(fileName);
		return new Tailer(file, listener, DEFAULT_DELAY, true, true);
	}

	public static String getlogs()
	{
		return logs.toString();
	}

	public static void addLog(final String line)
	{
		logs.insert(0, line + "\n");
	}

	public static void clearLogs()
	{
		logs.delete(0, logs.length());
	}
}
