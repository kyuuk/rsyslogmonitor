package me.kyuuk;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SyslogApplication
{
	private static ConfigurableApplicationContext context;

	public static void main(final String[] args)
	{
		final SpringApplication app = new SpringApplication(SyslogApplication.class);
		app.setWebApplicationType(WebApplicationType.SERVLET);
		app.addListeners(new ApplicationPidFileWriter());
		context = app.run(args);
	}

	public static void restart()
	{
		final ApplicationArguments args = context.getBean(ApplicationArguments.class);

		final Thread thread = new Thread(() -> {
			context.close();
			context = SpringApplication.run(SyslogApplication.class, args.getSourceArgs());
		});

		thread.setDaemon(false);
		thread.start();
	}
//	@Bean
//	public ApplicationRunner runner(@Qualifier("udpChannel") MessageChannel input)
//	{
//		return args -> {
//			while (true)
//			{
//				input.send(new MutableMessage<Map<String, Object>>(Collections.emptyMap()));
//				Thread.sleep(999999999);
//			}
//		};
//	}

}
