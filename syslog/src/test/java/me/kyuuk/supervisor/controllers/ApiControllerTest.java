package me.kyuuk.supervisor.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import me.kyuuk.supervisor.AbstractCommonWebTest;
import me.kyuuk.supervisor.bean.ExecutionState;
import me.kyuuk.supervisor.exceptions.ApplicationNotFound;
import me.kyuuk.supervisor.exceptions.ExecutorUnreachable;
import me.kyuuk.syslog.exception.InvalidParamException;
import me.kyuuk.web.ws.ApiPub;
import me.kyuuk.web.ws.bean.ApiResponse;
import me.kyuuk.web.ws.bean.ApiResponseCodes;

public class ApiControllerTest extends AbstractCommonWebTest
{

	@Autowired
	ApiPub controller;

	@Test
	public void refresh_success() throws Exception
	{
		final ApiResponse<ExecutionState> response = this.controller.refresh("valid", "valid");
		assertNotNull(response);
		assertEquals("exit code doesn't match", ApiResponseCodes.SUCCESS, response.getResponseCode());

		assertExecutionState(VALID_STATE, response.getResponse());
	}

	@Test(expected = ApplicationNotFound.class)
	public void refresh_ApplicationNotFound() throws Exception
	{
		this.controller.refresh("Invalid", "valid");
	}

	@Test(expected = InvalidParamException.class)
	public void refresh_InvalidParamAppName() throws Exception
	{
		this.controller.refresh(null, "valid");
	}

	@Test(expected = InvalidParamException.class)
	public void refresh_InvalidParamHost() throws Exception
	{
		this.controller.refresh("valid", null);
	}

	@Test(expected = ExecutorUnreachable.class)
	public void refresh_ExecutorUnreachable() throws Exception
	{
		this.controller.refresh("unreachable", "any");
	}

}
