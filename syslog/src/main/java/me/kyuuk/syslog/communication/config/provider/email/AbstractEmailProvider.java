package me.kyuuk.syslog.communication.config.provider.email;

import java.util.Properties;

import org.springframework.mail.javamail.JavaMailSenderImpl;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class AbstractEmailProvider
{
	private final String host;

	private final int port;

	private final String user;

	private final String password;

	private final boolean tls;

	private final String protocol;

	public abstract Properties getAdditionalProperties();

	public JavaMailSenderImpl getMailSender()
	{
		final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(getHost());
		mailSender.setPort(getPort());
		mailSender.setUsername(getUser());
		mailSender.setPassword(getPassword());
		final Properties mailProps = mailSender.getJavaMailProperties();
		mailProps.putAll(getAdditionalProperties());
		mailProps.put("mail.transport.protocol", getProtocol());
		mailProps.put("mail.smtp.starttls.enable", isTls());

		return mailSender;
	}
}
