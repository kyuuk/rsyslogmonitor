#!/bin/bash

###################################################################################
#										#
#	Author		:	Kyuk						#
#	Created		:	18/05/2019					#
#	Last-Modified	:	18/05/2019 Kyuk					#
#										#
###################################################################################

. ./setEnv.sh

EXEC_FILE="/etc/init.d/$NAME"

echo "[$(date)] --- DEBUT DU SCRIPT $0 --- "

#Check Root privilieges
check_Root () {
	if [ "$(id -u)" != "0" ]; then
			echo "this script must be run as root"
			exit 1
	fi
}
check_java () {
	if type -p java; then
		echo "Java is installed preceding with setup";
	else
		echo "Java is not present please install it";
		exit 1;
	fi
}

print_usage (){
        echo -e "usage:\n\t $0 {install|uninstall|reinstall}"
}

patch_exec_file () {
	echo "[$(date)] --- Patching init Script $NAME --- "
	installDirPatchString=#INSTALL_DIR#
	namePatchString=#NAME#
	userPatchString=#USER#
	cp $PWD/debian.init $PWD/debian.init.patched
	sed -i "s@$installDirPatchString@$INSTALL_DIR@g"  $PWD/debian.init.patched
	sed -i "s@$userPatchString@$RUN_AS@g"  $PWD/debian.init.patched
	sed -i "s@$namePatchString@$NAME@g"  $PWD/debian.init.patched
	chmod 764 $PWD/debian.init.patched
	chown $RUN_AS:$RUN_AS $PWD/debian.init.patched
	echo "[$(date)] --- Patching of $PWD/debian.init Done --- "
}

install () {
	echo "[$(date)] --- Begining install of $NAME --- "
	check_java
	patch_exec_file
	# copy exec file in /etc/init.d
	cp -f $PWD/debian.init.patched $EXEC_FILE
	#remove of patched file
	rm $PWD/debian.init.patched
	# enable service
	update-rc.d $NAME defaults

	# chech service
	service $NAME start
	service $NAME status
	if [ 0 -eq $? ]; then
		echo "[$(date)] --- $(tput setaf 2)Installation finished successfully$(tput sgr 0)"
	else
		echo "[$(date)] --- $(tput setaf 1)An error occured during install -> ROLLBACK $(tput sgr 0)"
		uninstall
	fi
}
uninstall () {
	echo "[$(date)] --- Removing $NAME --- "
	service $NAME stop 2>&1 /dev/null
	# disable service
	update-rc.d -f $NAME remove
	
	# delete symbolic link for service file in /etc/init.d
	rm $EXEC_FILE
	echo "[$(date)] --- $NAME removed --- "
}
########################################

check_Root

case "$1" in
	"install")
		install
		;;
	"uninstall")
		uninstall
		;;
	"reinstall")
		uninstall
		sleep 5
		install
		;;
	*)
		print_usage $0
		;;
esac
