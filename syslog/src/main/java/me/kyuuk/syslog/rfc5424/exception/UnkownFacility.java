package me.kyuuk.syslog.rfc5424.exception;

public class UnkownFacility extends Exception
{
	private static final long serialVersionUID = 1L;

	public UnkownFacility()
	{
	}

	public UnkownFacility(Integer key)
	{
		super("unable to find Facility with id " + key);
	}

}
