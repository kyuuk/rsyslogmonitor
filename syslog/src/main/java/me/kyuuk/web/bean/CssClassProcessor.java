package me.kyuuk.web.bean;

public class CssClassProcessor
{

	public enum SeverityClassMapping
	{
		EMERGENCY("Danger"), ALERT("Danger"), CRITICAL("Danger"), ERROR("Danger"), WARNING("Warning"), NOTICE("Info"),
		INFO("Info"), DEBUG("Light");

		private String cssClass;

		private SeverityClassMapping(String cssClass)
		{
			this.cssClass = "table-" + cssClass;
		}
	}

	public String getCssClassForLevel(String name)
	{
		return SeverityClassMapping.valueOf(name.toUpperCase()).cssClass;
	}

}
