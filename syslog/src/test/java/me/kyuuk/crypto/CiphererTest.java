package me.kyuuk.crypto;

import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

public class CiphererTest
{

	@Test
	public void cipherString() throws Exception
	{
		String content = RandomStringUtils.randomAscii(22);
		Cipherer c = new Cipherer("testCipherKey");
		String cipheredtoto = c.cipherString(content);
		String decipheredContent = c.decipherString(cipheredtoto);
		assertTrue("original content [" + content + "] is not equal de deciphered [" + decipheredContent + "]",
				StringUtils.equals(content, decipheredContent));
	}

}
