package me.kyuuk.syslog.communication.config.provider.email;

import java.util.Properties;

public class GmailProvider extends AbstractEmailProvider
{
	private static final Properties additionalProps = new Properties();

	static
	{
		additionalProps.put("mail.smtp.auth", true);
	}

	public GmailProvider(String user, String password)
	{
		super("smtp.gmail.com", 587, user, password, true, "smtp");
	}

	@Override
	public Properties getAdditionalProperties()
	{
		return additionalProps;
	}
}
