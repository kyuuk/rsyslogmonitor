package me.kyuuk.supervisor.exceptions;

public class ApplicationNotFound extends Exception
{

	public ApplicationNotFound()
	{
		super();
	}

	public ApplicationNotFound(String appName, String host)
	{
		super("Could not find application [" + appName + "] for host[" + host + "]");
	}

	public ApplicationNotFound(String appName, String host, Throwable t)
	{
		super("Could not find application [" + appName + "] for host[" + host + "]", t);
	}
}
