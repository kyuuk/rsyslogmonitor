package me.kyuuk.syslog.filter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.custom.UserConfigProvider;
import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;

@Component
@Log4j2
public class ApplicationFilter extends AbstractLogFilter
{
	private static final String IGNORED_APPS_KEY = "apps.ignored";

	private final List<String> application;

	@Autowired
	public ApplicationFilter(final UserConfigProvider userConfig)
	{
		super(userConfig);
		this.application = this.userConfig.getListProperty(IGNORED_APPS_KEY);
	}

	@Override
	public boolean shouldShowEvent(final SysLogEvent event)
	{
		if (isDisplayFilterEnabled())
		{
			log.debug("Display filter enabled. Checking inf app [{}] is filtered", event.getAppName());
			return !this.application.contains(event.getAppName());
		}
		return true;
	}

	@Override
	public boolean shouldStoreEvent(final SysLogEvent event)
	{
		if (isStorageFilterEnabled())
		{
			log.debug("Storage filter enabled. Checking if app [{}] is filtered", event.getAppName());
			return !this.application.contains(event.getAppName());
		}
		return true;
	}

	@Override
	public void refresh()
	{
		log.debug("Refreshing bean [{}]", this.getClass().getSimpleName());
		super.refresh();
		this.application.clear();
		this.application.addAll(this.userConfig.getListProperty(IGNORED_APPS_KEY));
	}

}
