package me.kyuuk.web.bean;

public class ModelKeys
{
	public static final String PAGE_TITLE = "pageTitle";
	public static final String ERROR_KEY = "error";
	public static final String EVENTS = "events";
	public static final String NOTIFICATION = "notification";
	public static final String NBR_NOTIFICATION = "nbrNotif";
	public static final String HOSTS = "hosts";
	public static final String HOST = "host";
	public static final String LOGS_PAGE_NUMBER = "pageNbr";
	public static final String PAGER = "pager";
	public static final String CONFIG = "props";
	public static final String LEVELS = "levels";
	public static final String APP_NAMES = "appNames";
	public static final String LOGS = "logs";
	public static final String APPLICATION_LOGS = "applicationLogsEnabled";
	public static final String METADATA = "metaData";
	public static final String APPLICATION_VERSION = "applicationVersion";
	public static final String CONTROLLER = "controller";
	public static final String ACTION = "action";
}
