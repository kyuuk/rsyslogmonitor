/**
 * 
 */
package me.kyuuk.supervisor.utils;

/**
 * @author kyuk
 *
 */
public class Constants
{
	private Constants()
	{
		// Nothing, constant Class
	}

	public static final String BASE_COMMAND = "/bin/bash";
	public static final String BASE_COMMAND_ARG = "-c";
	public static final String SSH_COMMAND = "ssh ";
	public static final String BASE_SERVICE_COMMAND = " /usr/sbin/service ";
	public static final String SERVICE_COMMAND = " sudo " + BASE_SERVICE_COMMAND;
	public static final String START = " start";
	public static final String STOP = " stop";
	public static final String STATUS = " status";
	public static final String RESTART = " restart";
	public static final String APPS_PROPERTY_KEY = "applications";

	/**
	 * Model Keys
	 */

}
