/**
 * 
 */
package me.kyuuk.supervisor.exceptions;

/**
 * @author kyuk
 *
 */
public class ExecutorUnreachable extends Exception
{
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public ExecutorUnreachable()
	{
		super();
	}

	public ExecutorUnreachable(String mesage)
	{
		super(mesage);
	}
}
