package me.kyuuk.syslog.server.config;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Service;

import me.kyuuk.syslog.communication.service.CommunicationManager;
import me.kyuuk.syslog.exception.InvalidSysLogEvent;
import me.kyuuk.syslog.filter.LogFilter;
import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;
import me.kyuuk.syslog.rfc5424.exception.UnknownSeverity;
import me.kyuuk.syslog.rfc5424.exception.UnkownFacility;
import me.kyuuk.syslog.server.HostOverseer;
import me.kyuuk.syslog.server.manager.SysLogEventManager;
import me.kyuuk.web.service.NotificationChecker;

@Service
public class SyslogEventHandler implements MessageHandler
{
	Logger logger = LogManager.getLogger(SyslogEventHandler.class);
	@Autowired
	private SysLogEventManager manager;

	@Autowired
	private CommunicationManager messageNotif;
	@Autowired
	private NotificationChecker notificator;

	@Autowired
	private HostOverseer hostSuplier;
	@Autowired
	private ApplicationContext context;

	@Override
	@ServiceActivator(inputChannel = "udpChannel")
	public void handleMessage(final Message<?> message)
	{
		@SuppressWarnings("unchecked")
		final Map<String, Object> data = (Map<String, Object>) message.getPayload();
		try
		{
			final SysLogEvent event = SysLogEvent.fromMap(data);
			boolean isEnabled = true;
			final Map<String, LogFilter> filters = this.context.getBeansOfType(LogFilter.class);
			for (final LogFilter filter : filters.values())
			{
				if (!filter.shouldStoreEvent(event))
				{
					isEnabled = false;
				}
			}
			if (isEnabled)
			{
				this.manager.insertEvent(event);
				this.hostSuplier.addHost(event.getHost());
				this.hostSuplier.addApp(event.getHost(), event.getAppName());
				if (NotificationChecker.isNotifiable(event))
				{
					this.notificator.addNotif(event);
					this.messageNotif
							.sendMessage("you have a new notifiation for " + event.getHost() + "\n with severity : "
									+ event.getSeverityText() + "\nwith message :\n" + event.getMessage());
				}
			}
		} catch (InvalidSysLogEvent | UnknownSeverity | UnkownFacility e)
		{
			this.logger.error("Could not parse SysLog event from Map[{}] cause : ", data, e);
		}

	}

}
