package me.kyuuk.syslog.rfc5424.bean;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.integration.syslog.SyslogHeaders;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import me.kyuuk.syslog.rfc5424.exception.UnknownSeverity;
import me.kyuuk.syslog.rfc5424.exception.UnkownFacility;

@Entity
@Data
@Log4j2
@Table(name = "SysLogEvent",
		indexes = { @Index(name = "Hostname", columnList = "host", unique = false),
				@Index(name = "severity", columnList = "severity", unique = false),
				@Index(name = "app", columnList = "appName", unique = false),
				@Index(name = "host_app", columnList = "host,appName", unique = false),
				@Index(name = "host_severity", columnList = "host,severity", unique = false) })
public class SysLogEvent implements Comparable<SysLogEvent>
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private long id;
	private Facility facility;
	private Severity severity;
	private String severityText;
	private LocalDateTime timeStamp;
	private String host;
	private String appName;
	private String procId;
	private String msgId;
	private Integer version;
	private String message;

	private SysLogEvent()
	{
	}

	public String getTimeStamp()
	{
		return this.timeStamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
	}

	public void setTimeStamp(final LocalDateTime timeStamp)
	{
		this.timeStamp = timeStamp;
	}

	public static SysLogEvent fromMap(final Map<String, Object> event) throws UnknownSeverity, UnkownFacility
	{
		final SysLogEvent sysLogEvent = new SysLogEvent();
		sysLogEvent.setFacility(Facility.fromId(MapUtils.getInteger(event, SyslogHeaders.FACILITY)));
		sysLogEvent.setSeverity(Severity.fromId(MapUtils.getInteger(event, SyslogHeaders.SEVERITY)));
		sysLogEvent.setSeverityText(MapUtils.getString(event, SyslogHeaders.SEVERITY_TEXT));
		sysLogEvent.setTimeStamp(getDateFromTimeStamp(MapUtils.getString(event, SyslogHeaders.TIMESTAMP)));
		sysLogEvent.setHost(MapUtils.getString(event, SyslogHeaders.HOST));
		sysLogEvent.setAppName(MapUtils.getString(event, SyslogHeaders.APP_NAME));
		sysLogEvent.setProcId(MapUtils.getString(event, SyslogHeaders.PROCID));
		sysLogEvent.setMsgId(MapUtils.getString(event, SyslogHeaders.MSGID));
		sysLogEvent.setVersion(MapUtils.getInteger(event, SyslogHeaders.VERSION));
		sysLogEvent.setMessage(MapUtils.getString(event, SyslogHeaders.MESSAGE));
		return sysLogEvent;
	}

	private static LocalDateTime getDateFromTimeStamp(final String ts)
	{
		if (StringUtils.isBlank(ts))
		{
			return null;
		}
		try
		{
			return LocalDateTime.parse(ts, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		} catch (final DateTimeParseException e)
		{
			log.error("Error while parsing timestamp [{}] : ", ts, e);
			return null;
		}
	}

	@Override
	public int compareTo(final SysLogEvent o)
	{
		return o.getTimeStamp().compareTo(getTimeStamp());
	}

}
