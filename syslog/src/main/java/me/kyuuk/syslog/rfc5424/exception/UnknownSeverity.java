package me.kyuuk.syslog.rfc5424.exception;

public class UnknownSeverity extends Exception
{
	private static final long serialVersionUID = 1L;

	public UnknownSeverity(Integer key)
	{
		super("unable to find Severity with id [" + key + "]");
	}

	public UnknownSeverity(String key)
	{
		super("unable to find Severity with name [" + key + "]");
	}

}
