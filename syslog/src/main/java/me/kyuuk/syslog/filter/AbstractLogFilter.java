package me.kyuuk.syslog.filter;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.custom.RefreshableBean;
import me.kyuuk.custom.UserConfigProvider;

@Log4j2
public abstract class AbstractLogFilter implements LogFilter, RefreshableBean
{
	private static final String DISPLAY_FILTER_KEY = "filter.display";
	private static final String STORAGE_FILTER_KEY = "filter.storage";
	protected UserConfigProvider userConfig;
	protected boolean isDisplayFilterEnabled;
	protected boolean isStorageFilterEnabled;

	public AbstractLogFilter(final UserConfigProvider userConfig)
	{
		this.userConfig = userConfig;
		init();
	}

	private final void init()
	{
		this.isDisplayFilterEnabled = this.userConfig.getBoolanProperty(DISPLAY_FILTER_KEY);
		this.isStorageFilterEnabled = this.userConfig.getBoolanProperty(STORAGE_FILTER_KEY);
		log.info("Init ApplicationLogFilter with values DisplayFilterEnabled[{}], StorageFilterEnabled[{}]",
				this.isDisplayFilterEnabled, this.isStorageFilterEnabled);
	}

	@Override
	public boolean isDisplayFilterEnabled()
	{
		return this.isDisplayFilterEnabled;
	}

	@Override
	public boolean isStorageFilterEnabled()
	{
		return this.isStorageFilterEnabled;
	}

	@Override
	public void refresh()
	{
		init();
	}
}
