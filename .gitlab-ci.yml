image: maven:latest

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=${CI_PROJECT_DIR}/.m2/repository"
  MAVEN_ADDITIONALS_OPTS: "-f syslog/"
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task

stages:
  - build
  - test
  - deploy-pages
  - package
  - docker-build
  - docker-push
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - ${CI_PROJECT_DIR}/.m2/repository/

include:
  - template: Code-Quality.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

.dind-config: &dind-config
  image: docker:latest
  cache: {}
  services:
    - docker:dind
  before_script:
    - echo $CI_BUILD_TOKEN | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  after_script:
    - docker logout $CI_REGISTRY

    
Compile:
  stage: build
  script:
    - mvn $MAVEN_OPTS compile $MAVEN_ADDITIONALS_OPTS
  only:
    - branches

Test:
  stage: test
  script:
    - mvn $MAVEN_OPTS test $MAVEN_ADDITIONALS_OPTS
    - cat ${CI_PROJECT_DIR}/syslog/target/site/jacoco/index.html
    - mv ${CI_PROJECT_DIR}/syslog/target/site/jacoco/ ${CI_PROJECT_DIR}/jacoco
  artifacts:
    expire_in: 1d
    paths:
        - ${CI_PROJECT_DIR}/jacoco
    reports:
      junit:
        - ${CI_PROJECT_DIR}/syslog/target/surefire-reports/TEST-*.xml
  only:
    - branches
    
sonarcloud-check:
  image: maven:3.6.3-jdk-11
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn $MAVEN_OPTS verify sonar:sonar -Dsonar.projectKey=kyuuk_rsyslogmonitor $MAVEN_ADDITIONALS_OPTS
  only:
    - merge_requests
    - master
    - develop

code_quality_html:
  extends: code_quality
  cache: {}
  variables:
    REPORT_FORMAT: html
  artifacts:
    paths: [gl-code-quality-report.html]
  rules:
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH'

code_quality:
  rules:
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH'

.hadoLint: &hadoLint
  image: hadolint/hadolint:latest-debian
  stage: test
  script:
    - hadolint ${DOCKERFILE}

Docker-Lint-armhf:
  variables:
    DOCKERFILE: Dockerfile.armhf
  <<: *hadoLint

Docker-Lint:
  variables:
    DOCKERFILE: Dockerfile
  <<: *hadoLint


Package:
  stage: package
  script:
    - mvn $MAVEN_OPTS package -DskipTests $MAVEN_ADDITIONALS_OPTS
    - mv ${CI_PROJECT_DIR}/syslog/target/RsysLogMonitor.jar ${CI_PROJECT_DIR}
  artifacts:
    expire_in: 7d
    paths:
        - ${CI_PROJECT_DIR}/RsysLogMonitor.jar
  rules:
    - if: $CI_COMMIT_BRANCH == 'develop'
      when: manual
    - if: $CI_COMMIT_TAG
    
.build_push_docker: &build_push_docker
  stage: docker-build
  <<: *dind-config
  script:
    - echo "Building image ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
    - docker build --pull -f ${DOCKERFILE} -t "${CI_REGISTRY_IMAGE}:${TAG}" .
    - docker push "${CI_REGISTRY_IMAGE}:${TAG}"

push_latest:
  stage : docker-push
  <<: *dind-config
  before_script:
    - echo $CI_BUILD_TOKEN | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  script:
    - docker pull ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
    - docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} ${CI_REGISTRY_IMAGE}:latest
    - docker push ${CI_REGISTRY_IMAGE}:latest
  only:
    - /^v[0-9]+\.[0-9]+\.[0-9]+$/

build_tag:
  variables:
    TAG: ${CI_COMMIT_TAG}
    DOCKERFILE: Dockerfile
  only:
    - tags
  <<: *build_push_docker

build_armhf:
  variables:
    TAG: ${CI_COMMIT_TAG}-armhf
    DOCKERFILE: Dockerfile.armhf
  tags: ["armhf"]
  only:
    - tags
  <<: *build_push_docker

pages:
  stage: deploy-pages
  script:
    - mvn $MAVEN_OPTS javadoc:javadoc $MAVEN_ADDITIONALS_OPTS
    - mkdir public
    - mv ${CI_PROJECT_DIR}/syslog/target/site/apidocs ${CI_PROJECT_DIR}/public/javadoc
    - mv ${CI_PROJECT_DIR}/jacoco ${CI_PROJECT_DIR}/public/jacoco
  artifacts:
    expire_in: 1d
    paths:
    - public
  only:
    - master