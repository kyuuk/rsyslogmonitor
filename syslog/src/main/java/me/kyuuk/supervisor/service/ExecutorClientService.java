/**
 * 
 */
package me.kyuuk.supervisor.service;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import me.kyuuk.custom.RefreshableBean;
import me.kyuuk.custom.UserConfigProvider;
import me.kyuuk.supervisor.bean.Application;
import me.kyuuk.supervisor.bean.ExecutionRequest;
import me.kyuuk.supervisor.bean.ExecutionState;
import me.kyuuk.supervisor.exceptions.ApplicationNotFound;
import me.kyuuk.supervisor.exceptions.ExecutorUnreachable;
import me.kyuuk.syslog.exception.InvalidParamException;

/**
 * @author kyuk
 *
 */
@Service
public class ExecutorClientService implements RefreshableBean
{
	private static final Logger logger = LogManager.getLogger(ExecutorClientService.class);

	private UserConfigProvider userConfigProvider;
	/**
	 * executor API authentication token
	 */
	private String token;
	/**
	 * executor Url
	 */
	private URI url;

	private String executorHostIp;
	private static final List<Application> apps = new ArrayList<>();

	/**
	 * 
	 */
	@Autowired
	public ExecutorClientService(UserConfigProvider userConfigProvider)
	{
		this.userConfigProvider = userConfigProvider;
		try
		{
			this.url = new URI(userConfigProvider.getProperty("executor.url"));
			this.token = userConfigProvider.getProperty("executor.token");
			this.executorHostIp = InetAddress.getByName(url.getHost()).getHostAddress();
		} catch (UnknownHostException | URISyntaxException e)
		{
			logger.error("Could not get executor Ip address");
		}
		logger.debug("current Executor host is [{}]", executorHostIp);
		initSupervisionApps();
	}

	private void initSupervisionApps()
	{
		List<String> hosts = this.userConfigProvider.getListProperty("hosts");
		logger.debug("initializing hosts {} applications", hosts);
		for (String host : hosts)
		{
			List<String> appsList = this.userConfigProvider.getListProperty(host + ".apps");
			initHostApps(appsList, host);
		}
	}

	private void initHostApps(List<String> applis, String host)
	{
		if (applis == null || applis.isEmpty())
		{
			logger.warn("Defined host [{}] has no applications", host);
			return;
		}
		for (String appName : applis)
		{
			try
			{
				String ip = InetAddress.getByName(host).getHostAddress();
				Application apli;
				appName = StringUtils.trim(appName);
				if (StringUtils.equalsIgnoreCase(ip, executorHostIp))
				{
					apli = new Application(appName, null);
				} else
				{
					apli = new Application(appName, host);
				}
				apps.add(apli);
			} catch (UnknownHostException e)
			{
				logger.error("Could not get ip for host [{}] not adding application [{}]", host, appName);
			}
		}
	}

	public ExecutionState getStatus(String appName, String host)
			throws ExecutorUnreachable, InvalidParamException, ApplicationNotFound
	{
		return execCommand(getApp(appName, host).getStatus());
	}

	public ExecutionState start(String appName, String host)
			throws ExecutorUnreachable, InvalidParamException, ApplicationNotFound
	{
		return execCommand(getApp(appName, host).getStart());
	}

	public ExecutionState stop(String appName, String host)
			throws ExecutorUnreachable, InvalidParamException, ApplicationNotFound
	{
		return execCommand(getApp(appName, host).getStop());
	}

	public ExecutionState restart(String appName, String host)
			throws ExecutorUnreachable, InvalidParamException, ApplicationNotFound
	{
		return execCommand(getApp(appName, host).getRestart());
	}

	public Map<Application, ExecutionState> getAllStatus() throws ExecutorUnreachable
	{
		Map<Application, ExecutionState> status = new HashMap<>();
		for (Application app : apps)
		{
			status.put(app, execCommand(app.getStatus()));
		}
		return status;
	}

	public ExecutionState execCommand(String command) throws ExecutorUnreachable
	{
		logger.info("Executing command [{}]", command);
		RestTemplate restTemplate = new RestTemplate();
		ExecutionRequest request = getRequestForCommand(command);
		ExecutionState response;
		try
		{
			response = restTemplate.postForObject(url, request, ExecutionState.class);
		} catch (RestClientException e)
		{
			throw new ExecutorUnreachable();
		}
		logger.debug("got Output [{}]", response.getOutput());
		logger.debug("got Error [{}]", response.getOutputErr());
		response.setStatus(getStatusFromOutput(response.getOutput()));
		return response;
	}

	private ExecutionRequest getRequestForCommand(String command)
	{
		ExecutionRequest request = new ExecutionRequest();
		request.setCommand(command);
		request.setToken(token);
		return request;
	}

	public Application getApp(String appName, String host) throws InvalidParamException, ApplicationNotFound
	{
		if (StringUtils.isAnyBlank(appName, host))
		{
			String theParam = appName == null ? "appName" : "host";
			logger.error("The param [{}]cannot be null", theParam);
			throw new InvalidParamException(theParam, null);
		}
		int appIndex = apps.indexOf(new Application(appName, host));
		if (appIndex >= 0)
		{
			return apps.get(appIndex);
		}
		throw new ApplicationNotFound(appName, host);
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	public URI getUrl()
	{
		return url;
	}

	public void setUrl(URI url)
	{
		this.url = url;
	}

	private static String getStatusFromOutput(String output)
	{
		if (StringUtils.isBlank(output))
		{
			return output;
		}
		String[] lines = output.split("\n");
		for (String line : lines)
		{
			logger.debug("getStatusFromOutput# Analyzing line [{}]", line);
			if (line.contains("Active:"))
			{
				logger.debug("getStatusFromOutput# found active line: [{}]", line);
				return StringUtils.trimToEmpty(line);
			}
		}
		logger.info("getStatusFromOutput# no active line returning output [{}]", output);
		return StringUtils.trimToEmpty(output);
//		return new BufferedReader(new StringReader(output)).lines().filter(line -> line.startsWith("Active:"))
//				.findFirst().orElse("");
	}

	@Override
	public void refresh()
	{
		logger.debug("Refreshing bean [{}]", this.getClass().getSimpleName());
		apps.clear();
		initSupervisionApps();
	}
}
