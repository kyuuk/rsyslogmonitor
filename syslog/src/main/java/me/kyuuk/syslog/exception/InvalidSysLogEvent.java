package me.kyuuk.syslog.exception;

import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;

public class InvalidSysLogEvent extends Exception
{

	public InvalidSysLogEvent(SysLogEvent event)
	{
		super("the event is null");
	}
}
