package me.kyuuk.custom;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public interface CustomPropertySource
{

	public void load();

	public void store() throws IOException;

	public String getProperty(String key);

	public Properties getAllProperties();

	public Map<String, String> getAllPrefixed(String prefix);

	public String getPropertyOrDefault(String key, String defaultValue);

	public void addProperty(String key, String value) throws IOException;

	public void addAll(Properties props) throws IOException;

	public void removeProperty(String key) throws IOException;

	public void refresh();
}
