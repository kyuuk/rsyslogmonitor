package me.kyuuk.log.audit;

public class AuditController extends Audit
{
	private final String action;

	public AuditController(final String action)
	{
		super();
		this.action = action;
	}

	@Override
	public String getAudit()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getComponent());
		sb.append(SEPARATOR);
		sb.append(action);
		sb.append(SEPARATOR);
		sb.append(getDuration());
		return sb.toString();
	}

	@Override
	public boolean isSystemError()
	{
		return false;
	}

	@Override
	public boolean isUserError()
	{
		return false;
	}

	@Override
	public String getComponent()
	{
		return "WEB";
	}

}
