package me.kyuuk;

import java.util.HashMap;
import java.util.Map;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.syslog.SyslogHeaders;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import me.kyuuk.syslog.server.respository.LogEventRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public abstract class AbstractCommonTest
{

	@Autowired
	protected LogEventRepository logEventRepository;

	public static final Map<String, Object> EVENT = getEventMap();

	protected static Map<String, Object> getEventMap()
	{
		Map<String, Object> params = new HashMap<>();
		params.put(SyslogHeaders.FACILITY, 19);
		params.put(SyslogHeaders.SEVERITY, 3);
		params.put(SyslogHeaders.SEVERITY_TEXT, "INFO");
		params.put(SyslogHeaders.APP_NAME, "test");
		params.put(SyslogHeaders.HOST, "localhost");
		params.put(SyslogHeaders.PROCID, "958");
		params.put(SyslogHeaders.MSGID, "0");
		params.put(SyslogHeaders.TIMESTAMP, "");
		params.put(SyslogHeaders.VERSION, "1");
		params.put(SyslogHeaders.MESSAGE, "test Message");
		return params;
	}
}
