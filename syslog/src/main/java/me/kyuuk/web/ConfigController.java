package me.kyuuk.web;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import me.kyuuk.web.bean.ConfigBackup;
import me.kyuuk.web.exception.BackupRestoreException;
import me.kyuuk.web.service.BackupService;

@Controller
@RequestMapping(path = "/config")
@ConditionalOnProperty(prefix = "logMonitor", name = "enabled")
public class ConfigController
{
	private static final Logger LOGGER = LogManager.getLogger();
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private final BackupService backupService;

	@Autowired
	public ConfigController(final BackupService backupService)
	{
		this.backupService = backupService;
	}

	@GetMapping(path = "/backup", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> backup() throws JsonProcessingException
	{
		final String jsonConfig = OBJECT_MAPPER.writerWithDefaultPrettyPrinter()
				.writeValueAsString(this.backupService.getBackup());
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=\"RsyslogMonitor_backup_"
								+ LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + ".json\"")
				.body(jsonConfig);
	}

	@PostMapping(path = "/restore")
	public RedirectView restore(@RequestParam("backup_file") final MultipartFile file)
			throws BackupRestoreException, IOException
	{
		this.backupService.restoreBackup(OBJECT_MAPPER.readValue(file.getBytes(), ConfigBackup.class));
		return new RedirectView("/config", true);
	}
}
