/**
 *
 */
package me.kyuuk.supervisor.bean;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import me.kyuuk.supervisor.utils.Constants;

/**
 * @author kyuk
 *
 */
@Data
@EqualsAndHashCode(of = { "host", "app" })
@ToString(of = { "host", "app" })
public final class Application
{
	private static final String SUDO = "%SUDO%";
	private final String app;
	private final String host;
	private final String baseCommand;

	/**
	 *
	 */
	public Application(final String appName, final String host)
	{
		this.app = appName;
		this.host = host;
		if (StringUtils.isBlank(host))
		{
			this.baseCommand = Constants.BASE_SERVICE_COMMAND + this.app;
		} else
		{
			this.baseCommand = Constants.SSH_COMMAND + host + Constants.BASE_SERVICE_COMMAND + this.app;
		}
	}

	/**
	 * @return the start
	 */
	public String getStart()
	{
		return StringUtils.replace(this.baseCommand, SUDO, "sudo") + Constants.START;
	}

	/**
	 * @return the stop
	 */
	public String getStop()
	{
		return StringUtils.replace(this.baseCommand, SUDO, "sudo") + Constants.STOP;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return this.baseCommand + Constants.STATUS;
	}

	/**
	 * @return the restart
	 */
	public String getRestart()
	{
		return StringUtils.replace(this.baseCommand, SUDO, "sudo") + Constants.RESTART;
	}

}
