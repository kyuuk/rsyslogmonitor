package me.kyuuk.syslog;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.IterableUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.integration.syslog.SyslogHeaders;

import me.kyuuk.AbstractCommonTest;
import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;
import me.kyuuk.syslog.rfc5424.exception.UnknownSeverity;
import me.kyuuk.syslog.rfc5424.exception.UnkownFacility;

public class SyslogApplicationTests extends AbstractCommonTest
{

	@Before
	public void clear()
	{
		logEventRepository.deleteAll();
	}

	@Test
	public void SavAndFindAll_success() throws UnknownSeverity, UnkownFacility
	{
		logEventRepository.save(SysLogEvent.fromMap(EVENT));
		logEventRepository.save(SysLogEvent.fromMap(EVENT));
		List<SysLogEvent> events = IterableUtils.toList(logEventRepository.findAll());
		assertNotNull(events);
		assertEquals(2, events.size());
	}

	@Test
	public void findAllHosts() throws UnknownSeverity, UnkownFacility
	{
		logEventRepository.save(SysLogEvent.fromMap(EVENT));
		Map<String, Object> copy = new HashMap<>(EVENT);
		copy.put(SyslogHeaders.HOST, "perso");
		logEventRepository.save(SysLogEvent.fromMap(copy));
		List<String> hosts = IterableUtils.toList(logEventRepository.getHosts());
		assertNotNull(hosts);
		assertEquals(2, hosts.size());
		assertTrue(hosts.contains("localhost"));
		assertTrue(hosts.contains("perso"));
	}
}
