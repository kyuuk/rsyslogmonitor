package me.kyuuk.log.audit;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Audit
{
	protected static final String SEPARATOR = "|";
	private static final Logger auditLogger = LogManager.getLogger("Audit");

	protected final long startTime = System.currentTimeMillis();

	public Audit()
	{

	}

	/** Retourne le log d'audit formaté sans les crochets */
	public abstract String getAudit();

	public abstract String getComponent();

	/** Pour les niveaux des logs */
	public abstract boolean isSystemError();

	public abstract boolean isUserError();

	public final void log()
	{
		if (this.isSystemError())
		{
			auditLogger.error(this.getAudit());
		} else if (this.isUserError())
		{
			auditLogger.warn(this.getAudit());
		} else
		{
			auditLogger.info(this.getAudit());
		}
	}

	public final long getDuration()
	{
		return System.currentTimeMillis() - startTime;
	}

	protected static String toLowerCase(final String string)
	{
		return valueOrBlank(StringUtils.lowerCase(string));
	}

	protected static String toUpperCase(final String string)
	{
		return valueOrBlank(StringUtils.upperCase(string));
	}

	protected static String valueOrBlank(final String string)
	{
		if (StringUtils.isBlank(string))
		{
			return "";
		}
		return string;
	}

	protected static Object valueOrBlank(final Object object)
	{
		if (object == null)
		{
			return "";
		}
		return object;
	}

	protected static String collectionToAuditString(final Iterable<?> collection, final String separator)
	{
		return StringUtils.join(collection, separator);
	}
}
