package me.kyuuk.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.custom.UserConfigProvider;
import me.kyuuk.log.audit.Audit;
import me.kyuuk.log.audit.AuditController;
import me.kyuuk.log.logtailer.LogTailer;
import me.kyuuk.supervisor.bean.Application;
import me.kyuuk.supervisor.bean.ExecutionState;
import me.kyuuk.supervisor.service.AppChecker;
import me.kyuuk.syslog.exception.InvalidParamException;
import me.kyuuk.syslog.rfc5424.bean.Severity;
import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;
import me.kyuuk.syslog.server.HostOverseer;
import me.kyuuk.syslog.server.manager.SysLogEventManager;
import me.kyuuk.web.bean.ModelKeys;
import me.kyuuk.web.bean.PagerModel;
import me.kyuuk.web.service.NotificationChecker;

@Log4j2
@Controller
@ConditionalOnProperty(prefix = "logMonitor", name = "enabled")
public class WebController
{

	private static final List<String> LEVELS = new ArrayList<>();

	private final MessageSource messageSource;
	private final SysLogEventManager manager;
	private final NotificationChecker notificator;
	private final AppChecker appChecker;
	private final UserConfigProvider configProvider;
	private final HostOverseer hostSupplier;

	@Value("${security.enabled}")
	private boolean sec;
	@Value("${server.servlet.context-path:/}")
	private String webappRoot;

	@Value("${application.version:0}")
	private String version;

	static
	{
		LEVELS.add("");
		LEVELS.addAll(Stream.of(Severity.values()).map(Severity::name).collect(Collectors.toList()));
	}

	@Autowired
	public WebController(final SysLogEventManager manager, final NotificationChecker notificator,
			final AppChecker appChecker, final UserConfigProvider configProvider, final HostOverseer hostSuplier,
			@Value("${logging.file:}") final String logFile, final MessageSource messageSource)
	{
		this.messageSource = messageSource;
		this.manager = manager;
		this.notificator = notificator;
		this.appChecker = appChecker;
		this.configProvider = configProvider;
		this.hostSupplier = hostSuplier;
		LogTailer.createLogTailer(logFile);
	}

	@GetMapping(path = { "/", "/index" })
	public String index(final Model model)
	{
		final Audit audit = new AuditController("get.index");
		try
		{
			setCommonattributes(model, "dashboard", "");
			final Map<Application, String> apps = this.appChecker.getSimpleStatus();
			model.addAttribute(ModelKeys.PAGE_TITLE, "Dashboard");
			model.addAttribute("apps", apps);
			return "index";
		} finally
		{
			audit.log();
		}
	}

	@GetMapping(path = "/login-page")
	public String login(final Model model)
	{
		final Audit audit = new AuditController("get.login");
		try
		{
			model.addAttribute(ModelKeys.PAGE_TITLE, "Login");
			return "login";
		} finally
		{
			audit.log();
		}
	}

	@GetMapping(path = "/logs")
	public String logs(final Model model, @RequestParam(name = "host", required = false) final String host,
			@RequestParam(name = "page", required = false) Integer page,
			@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "level", required = false) final String level,
			@RequestParam(name = "app", required = false) final String app)
	{
		final Audit audit = new AuditController("get.logs");
		Page<SysLogEvent> events = null;
		final List<String> appNames = new ArrayList<>();
		appNames.add("");
		try
		{
			setCommonattributes(model, "logs", "");
			model.addAttribute(ModelKeys.PAGE_TITLE, "Logs");

			if (page == null)
			{
				page = 1;
			}
			if (limit == null)
			{
				limit = 10;
			}
			if (StringUtils.isBlank(host))
			{
				events = this.manager.getAllPaged(page - 1, limit, level, app);
				appNames.addAll(this.hostSupplier.getApps());
			} else
			{
				events = this.manager.getByHostPaged(host, limit, page - 1, level, app);
				appNames.addAll(this.hostSupplier.getAppsForHost(host));
			}
			model.addAttribute(ModelKeys.LOGS_PAGE_NUMBER, events.getTotalPages());
			final PagerModel pager = new PagerModel(events.getTotalPages(), page, 11);
			model.addAttribute(ModelKeys.EVENTS, events.getContent());
			model.addAttribute(ModelKeys.PAGER, pager);
			model.addAttribute(ModelKeys.HOST, host);
			model.addAttribute(ModelKeys.LEVELS, LEVELS);
			model.addAttribute(ModelKeys.APP_NAMES, appNames);
			return "Logs";
		} catch (final InvalidParamException e)
		{
			log.error("An error occured returning nothing {}", e.getMessage());
			model.addAttribute(ModelKeys.ERROR_KEY, e.getMessage());
			return "Logs";
		} finally
		{
			audit.log();
		}

	}

	@PostMapping(path = "/clear")
	public String clearLogs(final Model model, @RequestParam(name = "host", required = false) final String host)
	{
		final Audit audit = new AuditController("post.clearLogs");
		try
		{
			if (StringUtils.isBlank(host))
			{
				this.hostSupplier.clearAll();
				this.manager.clearAll();
			} else if (StringUtils.equalsIgnoreCase("Notifications", host))
			{
				this.notificator.clearNotifs();
				if (this.notificator.haveNotif())
				{
					return notification(model);
				}
			} else
			{
				try
				{
					this.manager.clearByHost(host);
					this.hostSupplier.removeHost(host);
				} catch (final InvalidParamException e)
				{
					log.error("Parameter host is invalid [{}]", host, e);
				}
			}
			return "redirect:/logs";
		} finally
		{
			audit.log();
		}
	}

	@GetMapping(path = "/notification")
	public String notification(final Model model)
	{
		final Audit audit = new AuditController("get.notification");
		try
		{
			setCommonattributes(model, "notification", "");
			model.addAttribute(ModelKeys.PAGE_TITLE, "Notifications");
			final List<SysLogEvent> events = this.notificator.getNotifs();
			model.addAttribute("events", events);
			model.addAttribute(ModelKeys.HOST, "Notifications");
			return "Logs";
		} finally
		{
			audit.log();
		}
	}

	@GetMapping(path = "/status")
	public String status(final Model model, final Locale locale)
	{
		final Audit audit = new AuditController("get.status");
		try
		{
			log.info("Call to status page");
			setCommonattributes(model, "status", "");
			model.addAttribute(ModelKeys.PAGE_TITLE, "Status");
			Map<Application, ExecutionState> status;
			status = this.appChecker.getAllStatus();
			if (status.isEmpty())
			{
				model.addAttribute(ModelKeys.ERROR_KEY,
						this.messageSource.getMessage("Services.noservice", null, locale));
			} else
			{
				model.addAttribute("map", status);
			}
			return "status";
		} finally
		{
			audit.log();
		}
	}

	@GetMapping(path = "/config")
	public String configPage(final Model model)
	{
		final Audit audit = new AuditController("get.config");
		try
		{
			log.info("call to config page");
			setCommonattributes(model, "config", "");
			model.addAttribute(ModelKeys.PAGE_TITLE, "Configuration");
			log.info("getting all configuration");
			final Map<String, Object> config = new TreeMap<>(this.configProvider.getAllPropertiesAsMap());
			model.addAttribute(ModelKeys.CONFIG, hidePasswords(config));
			return "config";
		} finally
		{
			audit.log();
		}
	}

	@PostMapping(path = "/config")
	public String saveConfig(final Model model, final String propertyName, final String propertyValue)
	{
		final Audit audit = new AuditController("post.saveConfig");

		log.info("Submit of property [{}] to save with value [{}]", propertyName, propertyValue);
		try
		{
			this.configProvider.saveProperty(propertyName, propertyValue);
			return "redirect:/config";
		} catch (final IOException e)
		{
			model.addAttribute(ModelKeys.ERROR_KEY, "could not save the property " + propertyName);
			return configPage(model);
		} finally
		{
			audit.log();
		}
	}

	@GetMapping(path = "/config/delete")
	public String removeProperty(final Model model, final String propertyName)
	{
		final Audit audit = new AuditController("post.removeProperty");
		log.info("Submit of property [{}] for removal", propertyName);
		try
		{
			this.configProvider.removeProperty(propertyName);
			return "redirect:/config";
		} catch (final IOException e)
		{
			model.addAttribute(ModelKeys.ERROR_KEY, "could not remove the property [{}]" + propertyName);
			return configPage(model);
		} finally
		{
			audit.log();
		}
	}

	@GetMapping(path = "/hostStats")
	public String hostStats(final Model model, final String host)
	{
		final Audit audit = new AuditController("get.hostStats");
		try
		{
			log.info("Call to hostStats page");
			setCommonattributes(model, "hostStats", "");
			model.addAttribute(ModelKeys.PAGE_TITLE, "hostStats");
			model.addAttribute(ModelKeys.HOST, host);
			return "HostStats";
		} finally
		{
			audit.log();
		}
	}

	private void setCommonattributes(final Model model, final String controllerName, final String actionName)
	{
		model.addAttribute("securityEnabled", this.sec);
		model.addAttribute(ModelKeys.NOTIFICATION, this.notificator.haveNotif());
		model.addAttribute(ModelKeys.NBR_NOTIFICATION, this.notificator.getNbrNotif());
		model.addAttribute(ModelKeys.HOSTS, this.hostSupplier.getHosts());
		model.addAttribute(ModelKeys.APPLICATION_LOGS, !LogTailer.getlogs().isEmpty());
		model.addAttribute(ModelKeys.METADATA, getMetaData());
		model.addAttribute(ModelKeys.APPLICATION_VERSION, this.version);
		model.addAttribute(ModelKeys.CONTROLLER, controllerName);
		model.addAttribute(ModelKeys.ACTION, actionName);
	}

	private Map<String, String> getMetaData()
	{
		final Map<String, String> metadatas = new HashMap<>();
		metadatas.put("root", this.webappRoot);
		return metadatas;
	}

	private static Map<String, Object> hidePasswords(final Map<String, Object> toHide)
	{
		for (final String key : toHide.keySet())
		{
			if (StringUtils.containsIgnoreCase(key, "password"))
			{
				toHide.put(key, "**********");
			}
		}
		return toHide;
	}

	@GetMapping(path = "/applicationLog")
	public String applicationLog(final Model model)
	{
		final Audit audit = new AuditController("get.applicationLog");
		try
		{
			setCommonattributes(model, "appLogs", "");
			model.addAttribute(ModelKeys.PAGE_TITLE, "application logs");
			model.addAttribute(ModelKeys.LOGS, LogTailer.getlogs());
			return "applicationLog";
		} finally
		{
			audit.log();
		}
	}

}
