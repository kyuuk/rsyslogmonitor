package me.kyuuk.syslog.server.respository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import me.kyuuk.syslog.rfc5424.bean.Severity;
import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;

public interface LogEventRepository extends PagingAndSortingRepository<SysLogEvent, Long>
{
	@Query("SELECT e FROM SysLogEvent e WHERE e.host = :host")
	public List<SysLogEvent> findByHost(@Param("host") String host);

	@Query("SELECT e FROM SysLogEvent e WHERE e.host = :host")
	public Page<SysLogEvent> findByHost(@Param("host") String host, Pageable page);

	@Query("SELECT e FROM SysLogEvent e WHERE e.host = :host AND e.severity = :severity")
	public Page<SysLogEvent> findByHostSeverity(@Param("host") String host, @Param("severity") Severity severity,
			Pageable page);

	@Query("SELECT e FROM SysLogEvent e WHERE e.host = :host AND e.appName = :appName")
	public Page<SysLogEvent> findByHostAppName(@Param("host") String host, @Param("appName") String appName,
			Pageable page);

	@Query("SELECT e FROM SysLogEvent e WHERE e.host = :host AND e.severity = :severity AND e.appName = :appName")
	public Page<SysLogEvent> findByHostSeverityApp(@Param("host") String host, @Param("severity") Severity severity,
			@Param("appName") String appName, Pageable page);

	@Query("SELECT e FROM SysLogEvent e WHERE e.appName = :appName")
	public Page<SysLogEvent> findByAppName(@Param("appName") String appName, Pageable page);

	@Query("SELECT e FROM SysLogEvent e WHERE e.severity = :severity")
	public Page<SysLogEvent> findBySeverity(@Param("severity") Severity severity, Pageable page);

	@Query("SELECT e FROM SysLogEvent e WHERE e.severity = :severity AND e.appName = :appName")
	public Page<SysLogEvent> findBySeverityAppName(@Param("severity") Severity severity,
			@Param("appName") String appName, Pageable page);

	@Query("SELECT distinct host FROM SysLogEvent e")
	public List<String> getHosts();

	@Query("SELECT distinct appName FROM SysLogEvent e")
	public List<String> getApps();

	@Query("SELECT distinct appName FROM SysLogEvent e WHERE e.host = :host")
	public List<String> getAppsforHost(@Param("host") String host);

	@Query("SELECT COUNT(*) FROM SysLogEvent")
	public long getTotalNbr();

	@Query("SELECT COUNT(*) FROM SysLogEvent e WHERE e.host = :host")
	public int getTotalNbrByHost(@Param("host") String host);

	@Transactional
	@Modifying
	@Query("DELETE FROM SysLogEvent e where e.host = :host")
	public void clearByHost(@Param("host") String host);

	@Transactional
	@Modifying
	@Query("DELETE FROM SysLogEvent e where e.host = :host AND e.severity =:severity")
	public void clearByHostSeverity(@Param("host") String host, @Param("severity") Severity severity);

	public long countByHost(String host);

	public long countByHostAndSeverity(String host, Severity severity);

	public long countByHostAndAppName(String host, String appName);
}