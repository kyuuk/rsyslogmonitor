package me.kyuuk.log.audit;

public class AuditManager extends Audit
{
	private final String method;
	private boolean isSystemError;

	public AuditManager(final String method)
	{
		super();
		this.method = method;
	}

	@Override
	public String getAudit()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getComponent());
		sb.append(SEPARATOR);
		sb.append(method);
		sb.append(SEPARATOR);
		sb.append(getDuration());
		return sb.toString();
	}

	@Override
	public boolean isSystemError()
	{
		return this.isSystemError;
	}

	@Override
	public boolean isUserError()
	{
		return false;
	}

	public void setSystemError(boolean isSystemError)
	{
		this.isSystemError = isSystemError;
	}

	@Override
	public String getComponent()
	{
		return "MANAGER";
	}

}
