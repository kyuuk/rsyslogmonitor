package me.kyuuk.syslog.communication.config.provider.email;

import java.util.Properties;

import me.kyuuk.custom.UserConfigProvider;

public class CustomEmailProvider extends AbstractEmailProvider
{
	private static final Properties additionalProps = new Properties();

	public CustomEmailProvider(UserConfigProvider config)
	{
		this(config.getProperty("user.mail.server.host"), config.getIntProperty("user.mail.server.port"),
				config.getProperty("user.mail.sender.username"), config.getProperty("user.mail.sender.password"), true,
				"smtp");
	}

	public CustomEmailProvider(String host, int port, String user, String password, boolean tls, String protocol)
	{
		super(host, port, user, password, tls, protocol);
	}

	@Override
	public Properties getAdditionalProperties()
	{
		return additionalProps;
	}

}
