package me.kyuuk.syslog.exception;

public class InvalidParamException extends Exception
{
	public InvalidParamException(String paramName, String paramValue)
	{
		super("the parameter [" + paramName + "] with value [" + paramValue + "] is invalid");
	}

	public InvalidParamException(String paramName, String paramValue, Throwable t)
	{
		super("the parameter [" + paramName + "] with value [" + paramValue + "] is invalid", t);
	}

	public InvalidParamException()
	{
		super();
	}
}
