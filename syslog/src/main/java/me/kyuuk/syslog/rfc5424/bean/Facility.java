package me.kyuuk.syslog.rfc5424.bean;

import me.kyuuk.syslog.rfc5424.exception.UnkownFacility;

/**
 * @author Maikel
 *
 */
public enum Facility
{
	KERNEL_MSG(0, "kernel messages"), USER_MSG(1, "user-level messages"), MAIL(2, "mail system"),
	SYSTEM_DAEMON(3, "system daemons"), SECURITY(4, "security/authorization messages"),
	SYSLOGD(5, "messages generated internally by syslogd"), SUBSYSTEM(6, "line printer subsystem"),
	NETWORK_SUBSYSTEM(7, "network news subsystem"), UUCP(8, "UUCP subsystem"), CLOCK_DAEMON(9, "clock daemon"),
	SECURITY2(10, "security/authorization messages"), FTP(11, "FTP daemon"), NTP(12, "NTP subsystem"),
	AUDIT(13, "log audit"), ALERT(14, "log alert"), CLOCK2(15, "clock daemon(note 2)"), LOCAL0(16, "local use 0"),
	LOCAL1(17, "local use 1"), LOCAL2(18, "local use 2"), LOCAL3(19, "local use 3"), LOCAL4(20, "local use 4"),
	LOCAL5(21, "local use 5"), LOCAL6(22, "local use 6"), LOCAL7(23, "local use 7");

	private final int key;
	private final String label;

	private Facility(int key, String label)
	{
		this.key = key;
		this.label = label;
	}

	public static Facility fromId(Integer key) throws UnkownFacility
	{
		if (key == null)
		{
			throw new UnkownFacility(key);
		}
		for (Facility fac : Facility.values())
		{
			if (fac.getKey() == key.intValue())
				return fac;
		}
		throw new UnkownFacility(key);
	}

	public int getKey()
	{
		return key;
	}

	public String getLabel()
	{
		return label;
	}
}
