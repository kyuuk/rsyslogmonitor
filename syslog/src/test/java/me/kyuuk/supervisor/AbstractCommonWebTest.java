package me.kyuuk.supervisor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import me.kyuuk.supervisor.bean.ExecutionState;
import me.kyuuk.supervisor.exceptions.ApplicationNotFound;
import me.kyuuk.supervisor.exceptions.ExecutorUnreachable;
import me.kyuuk.supervisor.service.ExecutorClientService;
import me.kyuuk.syslog.exception.InvalidParamException;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public abstract class AbstractCommonWebTest
{
	public static final ExecutionState VALID_STATE = getValidExecutionState();
	public static final ObjectMapper MAPPER = new ObjectMapper();

	@Autowired
	private WebApplicationContext wac;
	protected MockMvc mockMvc;
	@MockBean
	public ExecutorClientService executor;

	@Before
	public void executorMock()
	{
		try
		{
			doReturn(AbstractCommonWebTest.VALID_STATE).when(this.executor).getStatus("valid", "valid");
			doThrow(new ApplicationNotFound("Invalid", "hosts")).when(this.executor).getStatus(eq("Invalid"),
					anyString());
			doThrow(new InvalidParamException()).when(this.executor).getStatus(isNull(), anyString());
			doThrow(new InvalidParamException()).when(this.executor).getStatus(anyString(), isNull());
			doThrow(new ExecutorUnreachable()).when(this.executor).getStatus(eq("unreachable"), anyString());
		} catch (final ExecutorUnreachable e)
		{
			e.printStackTrace();
		} catch (final InvalidParamException e)
		{
			e.printStackTrace();
		} catch (final ApplicationNotFound e)
		{
			e.printStackTrace();
		}
	}

	@Before
	public void setup()
	{
		final DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
		this.mockMvc = builder.build();
	}

	private static ExecutionState getValidExecutionState()
	{
		final ExecutionState state = new ExecutionState(0, TestConstants.OUTPUT, TestConstants.OUTPUT_ERROR);
		state.setStatus(TestConstants.STATUS_LINE);
		return state;
	}

	public static void assertExecutionState(final ExecutionState expected, final ExecutionState actual)
	{
		assertNotNull("L'object state actual est null", actual);
		assertEquals("l'exit code n'est pas le meme ", expected.getExitCode(), actual.getExitCode());
		assertEquals("L'output n'est pas le meme ", expected.getOutput(), actual.getOutput());
		assertEquals("Le status n'est pas le meme ", expected.getStatus(), actual.getStatus());
		assertEquals("L'outpuError n'est pas le meme ", expected.getOutputErr(), actual.getOutputErr());
	}

}
