package me.kyuuk.web.ws.bean;

public enum ApiResponseCodes
{
	UNAVAIBLABLE_SYS(-1, "systeme indisponible"), SUCCESS(0, "Success"),
	EXECUTOR_UNREACHABLE(1, "le service d'execution de commandes n'est pas joignable"),
	INVALID_PARAM(2, "un parametre de la requete est invalide"),
	APPLICATION_NOT_FOUND(3, "L'application demandé n'existe pas");

	private final int code;
	private final String message;

	private ApiResponseCodes(int code, String message)
	{
		this.code = code;
		this.message = message;
	}

	public int getCode()
	{
		return code;
	}

	public String getMessage()
	{
		return message;
	}

}
