package me.kyuuk.custom;

public interface RefreshableBean
{
	public void refresh();
}
