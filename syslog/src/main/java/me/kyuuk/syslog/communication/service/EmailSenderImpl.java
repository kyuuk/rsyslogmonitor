package me.kyuuk.syslog.communication.service;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import me.kyuuk.custom.UserConfigProvider;

public class EmailSenderImpl implements CommunicationManager
{
	private static final Logger logger = LogManager.getLogger(EmailSenderImpl.class);
	private final UserConfigProvider config;
	private final AtomicReference<JavaMailSenderImpl> mailSender = new AtomicReference<>();
	private final AtomicReference<String> sendTo = new AtomicReference<>();

	public EmailSenderImpl(UserConfigProvider config)
	{
		this.config = config;
		this.mailSender.set(this.config.getMailSender());
		sendTo.set(config.getProperty("user.mail.notification.adress"));
	}

	@Override
	public void sendMessage(final String text)
	{
		if (config.getBoolanProperty("logMonitor.notification.message.enabled"))
		{
			SimpleMailMessage message = getMessageTemplate();
			message.setText(text);
			logger.info("sending email notification to [{}]", message.getTo());
			mailSender.get().send(message);
		}
	}

	private SimpleMailMessage getMessageTemplate()
	{
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject("Rsyslog Monitor Notification");
		message.setTo(sendTo.get());
		message.setFrom(mailSender.get().getUsername());
		return message;
	}

	@Override
	public void refresh()
	{
		logger.debug("Refreshing bean [{}]", this.getClass().getSimpleName());
		sendTo.set(config.getProperty("logMonitor.notification.message.sendto"));
		this.mailSender.set(this.config.getMailSender());
	}
}
