package me.kyuuk.syslog.server.manager.bean;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import me.kyuuk.syslog.rfc5424.bean.Severity;

@Data
public class LogCount
{
	private static final List<Severity> errorLevels = Arrays.asList(Severity.ERROR, Severity.ALERT, Severity.CRITICAL);
	private final String host;
	private final Map<Severity, Long> counts = new EnumMap<>(Severity.class);

	public LogCount(final String host)
	{
		this.host = host;
	}

	public long getTotal()
	{
		long total = 0;
		for (final long count : this.counts.values())
		{
			total += count;
		}
		return total;
	}

	public long getErrors()
	{
		long errorCount = 0;
		for (final Severity level : errorLevels)
		{
			errorCount += this.counts.get(level) != null ? this.counts.get(level) : 0;
		}
		return errorCount;
	}

	public void addCount(final Severity severity, final Long count)
	{
		if (count != 0)
		{
			this.counts.put(severity, count);
		}
	}

}
