package me.kyuuk.web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;

@Service
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class NotificationChecker
{
	private Object mutex = new Object();
	private boolean haveNotif = false;
	private int nbrNew = 0;
	private List<SysLogEvent> toNotify = new ArrayList<>();

	public void addNotif(SysLogEvent event)
	{
		synchronized (mutex)
		{
			this.toNotify.add(0, event);
			this.haveNotif = true;
			this.nbrNew++;
		}
	}

	public void clearNotifs()
	{
		synchronized (mutex)
		{
			if (haveNotif)
			{
				if (nbrNew == 0)
				{
					this.toNotify.clear();
					this.haveNotif = false;
					this.nbrNew = 0;
				} else
				{
					clearOldNotifs();
				}
			}
		}
	}

	public List<SysLogEvent> getNotifs()
	{
		synchronized (mutex)
		{
			this.nbrNew = 0;
			return toNotify;
		}
	}

	private void clearOldNotifs()
	{
		while (toNotify.size() > nbrNew)
		{
			toNotify.remove(0);
		}
	}

	public boolean haveNotif()
	{
		return this.haveNotif;
	}

	public int getNbrNotif()
	{
		return this.nbrNew;
	}

	public static boolean isNotifiable(SysLogEvent event)
	{
		switch (event.getSeverity())
		{
			case WARNING:
			case INFO:
			case NOTICE:
			case DEBUG:
				return false;
			default:
				return true;
		}
	}
}
