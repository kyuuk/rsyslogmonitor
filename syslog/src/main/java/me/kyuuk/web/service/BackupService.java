package me.kyuuk.web.service;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import me.kyuuk.custom.UserConfigProvider;
import me.kyuuk.web.bean.ConfigBackup;
import me.kyuuk.web.exception.BackupRestoreException;

@Service
public class BackupService
{
	private final UserConfigProvider userConfig;
	private final String appVersion;

	@Autowired
	public BackupService(final UserConfigProvider userConfig,
			@Value("${application.version:0}") final String appVersion)
	{
		this.userConfig = userConfig;
		this.appVersion = appVersion;
	}

	public ConfigBackup getBackup()
	{
		final ConfigBackup backup = new ConfigBackup();
		backup.setAppVersion(this.appVersion);
		backup.setConfig(this.userConfig.getAllPropertiesAsMap());
		return backup;
	}

	public void restoreBackup(final ConfigBackup config) throws BackupRestoreException
	{
		if (config == null || config.getConfig() == null || config.getConfig().isEmpty())
		{
			throw new BackupRestoreException("No config provided or it is empty");
		}
		try
		{
			final Properties props = new Properties();
			props.putAll(config.getConfig());
			this.userConfig.saveAllProperties(props);
		} catch (final IOException e)
		{
			throw new BackupRestoreException("Error while restoring Config", e);
		}
	}
}
