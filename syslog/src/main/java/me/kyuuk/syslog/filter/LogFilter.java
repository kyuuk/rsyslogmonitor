package me.kyuuk.syslog.filter;

import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;

public interface LogFilter
{

	/**
	 * Getter for the display filtering of logs
	 *
	 * @return boolean
	 */
	public boolean isDisplayFilterEnabled();

	public boolean isStorageFilterEnabled();

	public boolean shouldShowEvent(SysLogEvent event);

	public boolean shouldStoreEvent(SysLogEvent event);
}
