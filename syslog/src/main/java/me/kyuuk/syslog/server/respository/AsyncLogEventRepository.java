package me.kyuuk.syslog.server.respository;

import static me.kyuuk.syslog.server.AsyncDaoRunner.run;

import org.springframework.stereotype.Component;

import me.kyuuk.syslog.rfc5424.bean.Severity;
import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;

@Component
public class AsyncLogEventRepository
{

	private final LogEventRepository repo;

	public AsyncLogEventRepository(LogEventRepository repo)
	{
		this.repo = repo;
	}

	public void clearByHost(String host)
	{
		run("clearByHost", () -> repo.clearByHost(host), () -> "[host:" + host + "]");
	}

	public void insertEvent(SysLogEvent event)
	{
		run("insertEvent", () -> repo.save(event), () -> "");
	}

	public void clearByHostSeverity(String host, Severity severity)
	{
		run("clearByHostSeverity", () -> repo.clearByHostSeverity(host, severity),
				() -> "[host:" + host + "],[severity:" + severity + "]");
	}

	public void clearAll()
	{
		run("clearByHost", repo::deleteAll, () -> "");
	}
}
