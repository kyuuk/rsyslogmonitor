/**
 *
 */
package me.kyuuk.supervisor.bean;

import lombok.Data;

/**
 * @author kyuk
 *
 */
@Data
public class ExecutionRequest
{
	/**
	 * The command to execute
	 */
	private String command;
	/**
	 * API Authentication token
	 */
	private String token;
}
