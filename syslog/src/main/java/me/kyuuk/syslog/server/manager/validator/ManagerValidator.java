package me.kyuuk.syslog.server.manager.validator;

import org.apache.commons.lang3.StringUtils;

import me.kyuuk.syslog.exception.InvalidParamException;
import me.kyuuk.syslog.exception.InvalidSysLogEvent;
import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;

public class ManagerValidator
{
	public static void event(SysLogEvent event) throws InvalidSysLogEvent
	{
		if (event == null)
		{
			throw new InvalidSysLogEvent(event);
		}
	}

	public static void host(String host) throws InvalidParamException
	{
		if (StringUtils.isBlank(host))
		{
			throw new InvalidParamException("host", host);
		}

	}

	public static void appName(String appName) throws InvalidParamException
	{
		if (StringUtils.isBlank(appName))
		{
			throw new InvalidParamException("appName", appName);
		}

	}

	public static void startLimits(int start, int limit) throws InvalidParamException
	{
		if (start < 0 || limit <= 0)
		{
			throw new InvalidParamException("start || limit ", start + " || " + limit);
		}
	}

	public static void limit(int limit) throws InvalidParamException
	{
		if (limit <= 0)
		{
			throw new InvalidParamException("limit ", "" + limit);
		}
	}
}
