package me.kyuuk.syslog.server.manager.bean;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class LogCountByApp
{

	private final String host;
	private final Map<String, Long> countByApp = new HashMap<>();

	public LogCountByApp(final String host)
	{
		this.host = host;
	}

	public void addAppCount(final String appName, final Long count)
	{
		this.countByApp.put(appName, count);
	}

	public long getTotal()
	{
		long total = 0;
		for (final long count : this.countByApp.values())
		{
			total += count;
		}
		return total;
	}

}
