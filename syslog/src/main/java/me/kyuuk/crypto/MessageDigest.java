package me.kyuuk.crypto;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SHA512Digest;

public class MessageDigest
{

	private final Digest digest;

	/**
	 * Créé un objet permettant de réaliser un Hash SHA1 de texte
	 */
	public MessageDigest()
	{
		this("SHA1");
	}

	/**
	 * Créé un objet permettant de réaliser un Hash de texte
	 * 
	 * @param algo algorithme de Hash a utiliser. Supporte :
	 *             <ul>
	 *             <li>SHA1 <i>(valeur par défaut si algorithme non trouvé)</i></li>
	 *             <li>SHA256</li>
	 *             <li>SHA512</li>
	 *             <li>MD5</li>
	 *             </ul>
	 */
	public MessageDigest(final String algo)
	{
		CryptoCommons.addBouncyCastleProvider();
		String theAlgo = algo;
		if (StringUtils.isBlank(algo))
		{
			theAlgo = "";
		}

		if ("SHA256".equalsIgnoreCase(theAlgo))
		{
			this.digest = new SHA256Digest();
		} else if ("SHA".equalsIgnoreCase(theAlgo) || "SHA512".equalsIgnoreCase(theAlgo))
		{
			this.digest = new SHA512Digest();
		} else if ("MD5".equalsIgnoreCase(theAlgo))
		{
			this.digest = new MD5Digest();
		} else
		{
			this.digest = new SHA1Digest();
		}
	}

	/**
	 * Calcul de Hash de message (le message est considéré comme étant en UTF-8)
	 * 
	 * @param message message à hasher
	 * @return retourne un tableau de byte
	 */
	public byte[] digest(final String message)
	{
		return this.digest(message, "UTF-8");
	}

	/**
	 * Calcul de Hash de message
	 * 
	 * @param message  message à hasher
	 * @param encoding encodage à utiliser
	 * @return retourne un tableau de byte
	 */
	public byte[] digest(final String message, final String encoding)
	{
		if (message == null)
		{
			return new byte[0];
		}

		final byte[] retValue = new byte[this.digest.getDigestSize()];
		final byte[] msg = CryptoCommons.getBytes(message, encoding);
		if (msg.length == 0)
		{
			return new byte[0];
		}

		this.digest.update(msg, 0, msg.length);
		this.digest.doFinal(retValue, 0);
		return retValue;
	}
}
