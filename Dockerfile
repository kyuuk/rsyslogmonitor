FROM openjdk:11.0.11-jre-slim

RUN mkdir -p /config /app/RsyslogMonitor
VOLUME /config
COPY docker/config.properties.default /config/config.properties
COPY docker/user.config.default /config/user.config

WORKDIR /app/RsyslogMonitor
COPY RsysLogMonitor.jar /app/RsyslogMonitor/app.jar

ENTRYPOINT ["java","-jar","/app/RsyslogMonitor/app.jar","--spring.config.location=/config/config.properties"]
EXPOSE 8080 514