/**
 *
 */
package me.kyuuk.web.config;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.web.bean.ModelKeys;

/**
 * @author kyuk
 *
 */
@Controller
@Log4j2
public class CustomErrorController implements ErrorController
{
	private static final String ERROR_ATTRIBUTE = DefaultErrorAttributes.class.getName() + ".ERROR";
	@Autowired
	Environment env;

	@GetMapping(path = "/error")
	public String handleError(final Model model, final HttpServletRequest request)
	{
		final Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		if (Arrays.asList(this.env.getActiveProfiles()).contains("development"))
		{
			log.debug("developement profile detected returning error_DEV page");
			model.addAllAttributes(getErrorAttributes(request, true));
			return "error_DEV";
		}
		if (status != null)
		{
			final Integer statusCode = Integer.valueOf(status.toString());

			if (statusCode == HttpStatus.NOT_FOUND.value())
			{
				return "error-404";
			} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value())
			{
				return "error-500";
			}
		}
		return ModelKeys.ERROR_KEY;
	}

	@Override
	public String getErrorPath()
	{
		return "/error";
	}

	public Map<String, Object> getErrorAttributes(final HttpServletRequest request, final boolean includeStackTrace)
	{
		final Map<String, Object> errorAttributes = new LinkedHashMap<>();
		final WebRequest webRequest = new ServletWebRequest(request);
		errorAttributes.put("timestamp", new Date());
		addStatus(errorAttributes, webRequest);
		addErrorDetails(errorAttributes, webRequest, includeStackTrace);
		addPath(errorAttributes, webRequest);
		return errorAttributes;
	}

	private void addErrorDetails(final Map<String, Object> errorAttributes, final WebRequest webRequest,
			final boolean includeStackTrace)
	{
		Throwable error = getError(webRequest);
		if (error != null)
		{
			while (error instanceof ServletException && error.getCause() != null)
			{
				error = ((ServletException) error).getCause();
			}
			errorAttributes.put("exception", error.getClass().getName());
			addErrorMessage(errorAttributes, error);
			if (includeStackTrace)
			{
				addStackTrace(errorAttributes, error);
			}
		}
		final Object message = getAttribute(webRequest, "javax.servlet.error.message");
		if ((!StringUtils.isEmpty(message) || errorAttributes.get("message") == null)
				&& !(error instanceof BindingResult))
		{
			errorAttributes.put("message", StringUtils.isEmpty(message) ? "No message available" : message);
		}
	}

	private void addStackTrace(final Map<String, Object> errorAttributes, final Throwable error)
	{
		final StringWriter stackTrace = new StringWriter();
		error.printStackTrace(new PrintWriter(stackTrace));
		stackTrace.flush();
		errorAttributes.put("trace", stackTrace.toString());
	}

	private void addPath(final Map<String, Object> errorAttributes, final RequestAttributes requestAttributes)
	{
		final String path = getAttribute(requestAttributes, "javax.servlet.error.request_uri");
		if (path != null)
		{
			errorAttributes.put("path", path);
		}
	}

	public Throwable getError(final WebRequest webRequest)
	{
		Throwable exception = getAttribute(webRequest, ERROR_ATTRIBUTE);
		if (exception == null)
		{
			exception = getAttribute(webRequest, "javax.servlet.error.exception");
		}
		return exception;
	}

	private void addStatus(final Map<String, Object> errorAttributes, final RequestAttributes requestAttributes)
	{
		final Integer status = getAttribute(requestAttributes, "javax.servlet.error.status_code");
		if (status == null)
		{
			errorAttributes.put("status", 999);
			errorAttributes.put(ModelKeys.ERROR_KEY, "None");
			return;
		}
		errorAttributes.put("status", status);
		try
		{
			errorAttributes.put(ModelKeys.ERROR_KEY, HttpStatus.valueOf(status).getReasonPhrase());
		} catch (final Exception ex)
		{
			// Unable to obtain a reason
			errorAttributes.put(ModelKeys.ERROR_KEY, "Http Status " + status);
		}
	}

	private BindingResult extractBindingResult(final Throwable error)
	{
		if (error instanceof BindingResult)
		{
			return (BindingResult) error;
		}
		if (error instanceof MethodArgumentNotValidException)
		{
			return ((MethodArgumentNotValidException) error).getBindingResult();
		}
		return null;
	}

	private void addErrorMessage(final Map<String, Object> errorAttributes, final Throwable error)
	{
		final BindingResult result = extractBindingResult(error);
		if (result == null)
		{
			errorAttributes.put("message", error.getMessage());
			return;
		}
		if (result.hasErrors())
		{
			errorAttributes.put("errors", result.getAllErrors());
			errorAttributes.put("message", "Validation failed for object='" + result.getObjectName()
					+ "'. Error count: " + result.getErrorCount());
		} else
		{
			errorAttributes.put("message", "No errors");
		}
	}

	@SuppressWarnings("unchecked")
	private <T> T getAttribute(final RequestAttributes requestAttributes, final String name)
	{
		return (T) requestAttributes.getAttribute(name, RequestAttributes.SCOPE_REQUEST);
	}

	/**
	 * TODO a voir peu etre plus simple
	 */
//	@RequestMapping
//	  public Map<String, Object> error(HttpServletRequest aRequest){
//	     Map<String, Object> body = getErrorAttributes(aRequest,getTraceParameter(aRequest));
//	     String trace = (String) body.get("trace");
//	     if(trace != null){
//	       String[] lines = trace.split("\n\t");
//	       body.put("trace", lines);
//	     }
//	     return body;
//	  }
//
//	  private boolean getTraceParameter(HttpServletRequest request) {
//	    String parameter = request.getParameter("trace");
//	    if (parameter == null) {
//	        return false;
//	    }
//	    return !"false".equals(parameter.toLowerCase());
//	  }
//
//	  private Map<String, Object> getErrorAttributes(HttpServletRequest aRequest, boolean includeStackTrace) {
//	    RequestAttributes requestAttributes = new ServletRequestAttributes(aRequest);
//	    return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
//	  }
}
