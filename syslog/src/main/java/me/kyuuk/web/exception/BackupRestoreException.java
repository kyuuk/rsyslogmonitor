package me.kyuuk.web.exception;

public class BackupRestoreException extends Exception
{

	public BackupRestoreException()
	{
	}

	public BackupRestoreException(final String message)
	{
		super(message);
	}

	public BackupRestoreException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public BackupRestoreException(final Throwable cause)
	{
		super(cause);
	}
}
