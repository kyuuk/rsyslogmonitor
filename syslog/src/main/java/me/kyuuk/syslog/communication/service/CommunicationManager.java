package me.kyuuk.syslog.communication.service;

import me.kyuuk.custom.RefreshableBean;

public interface CommunicationManager extends RefreshableBean
{

	public void sendMessage(final String text);
}
