package me.kyuuk.web.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.supervisor.bean.ExecutionState;
import me.kyuuk.supervisor.exceptions.ApplicationNotFound;
import me.kyuuk.supervisor.exceptions.ExecutorUnreachable;
import me.kyuuk.supervisor.service.AppChecker;
import me.kyuuk.syslog.exception.InvalidParamException;
import me.kyuuk.syslog.server.manager.SysLogEventManager;
import me.kyuuk.syslog.server.manager.bean.LogCount;
import me.kyuuk.syslog.server.manager.bean.LogCountByApp;
import me.kyuuk.web.ws.bean.ApiResponse;
import me.kyuuk.web.ws.bean.ApiResponseCodes;

@Log4j2
@RestController
@RequestMapping(path = "/api")
@ConditionalOnProperty(prefix = "logMonitor", name = "enabled")
public class ApiPub
{

	private final AppChecker appChecker;
	private final SysLogEventManager manager;

	@Autowired
	public ApiPub(final SysLogEventManager manager, final AppChecker appChecker)
	{
		this.manager = manager;
		this.appChecker = appChecker;
	}

	@PostMapping("/statsByHost")
	public ApiResponse<LogCount> countByHost(final String host)
	{
		final LogCount count = this.manager.getCountsForHost(host);
		return new ApiResponse<>(ApiResponseCodes.SUCCESS, count);
	}

	@GetMapping("/getLogStats")
	public ApiResponse<List<LogCount>> countAll()
	{
		return new ApiResponse<>(ApiResponseCodes.SUCCESS, this.manager.getAllCounts());
	}

	@PostMapping(path = "/refresh")
	public ApiResponse<ExecutionState> refresh(final String appName, final String host)
			throws ExecutorUnreachable, InvalidParamException, ApplicationNotFound
	{
		log.info("Getting status for app [{}] on host [{}]", appName, host);
		return new ApiResponse<>(ApiResponseCodes.SUCCESS, this.appChecker.forceRefresh(appName, host));
	}

	@PostMapping("/getAppStatsForHost")
	public ApiResponse<LogCountByApp> countLogsByAppForHost(final String host)
	{
		return new ApiResponse<>(ApiResponseCodes.SUCCESS, this.manager.getCountsForHostApp(host));
	}

	@PostMapping("/getlevelStatsForHost")
	public ApiResponse<LogCount> countLogsBySeverityForHost(final String host)
	{
		return new ApiResponse<>(ApiResponseCodes.SUCCESS, this.manager.getCountsForHost(host));
	}
}
