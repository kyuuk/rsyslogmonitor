/**
 *
 */
package me.kyuuk.supervisor.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author kyuk
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ExecutionState extends ExecutionStatus
{
	private String status;

	public ExecutionState()
	{
		this(-1, null, null);
	}

	public ExecutionState(final int exitcode, final String output)
	{
		this(exitcode, output, null);
	}

	public ExecutionState(final int exitcode, final String output, final String outErr)
	{
		super(exitcode, output, outErr);
	}

}
