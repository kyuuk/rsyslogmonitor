/**
 *
 */
package me.kyuuk.supervisor.bean;

import lombok.Data;

/**
 * @author kyuk
 *
 */
@Data
public class ExecutionStatus
{

	/**
	 * the exit code returned by a command execution
	 */
	private final int exitCode;
	/**
	 * the Output of a command execution
	 */
	private final String output;
	/**
	 * the error Output of a command execution
	 */
	private final String outputErr;

	/**
	 *
	 * @param exitcode the exitCode of the command execution
	 * @param output   the output of the command execution
	 * @param err      stdErr output for the command execution
	 */
	public ExecutionStatus(final int exitcode, final String output, final String err)
	{
		this.exitCode = exitcode;
		this.output = output;
		this.outputErr = err;
	}
}
