package me.kyuuk.custom.exception;

public class InitializationException extends RuntimeException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InitializationException()
	{
		super();
	}

	public InitializationException(String message)
	{
		super(message);
	}

	public InitializationException(String message, Throwable t)
	{
		super(message, t);
	}
}
