package me.kyuuk.web.bean;

import java.util.Arrays;
import java.util.List;

public class PagerModel
{

	private int buttonsToShow = 5;
	private List<Integer> pageSizes = Arrays.asList(10, 20, 30, 40, 50, 100);
	private int startPage;
	private int endPage;
	private boolean showBefore;
	private boolean showAfter;
	private int totalPages;
	private int currentPage;

	public PagerModel(int totalPages, int currentPage, int buttonsToShow)
	{
		this.currentPage = currentPage;
		this.totalPages = totalPages;
		setButtonsToShow(buttonsToShow);
		int halfPagesToShow = getButtonsToShow() / 2;

		if (totalPages <= getButtonsToShow())
		{
			setStartPage(1);
			setEndPage(totalPages);
			this.showBefore = false;
			this.showAfter = false;
		} else if (currentPage - halfPagesToShow <= 0)
		{
			setStartPage(1);
			setEndPage(getButtonsToShow());
			this.showBefore = false;
			this.showAfter = true;
		} else if (currentPage + halfPagesToShow == totalPages)
		{
			setStartPage(currentPage - halfPagesToShow);
			setEndPage(totalPages);
			this.showBefore = true;
			this.showAfter = false;
		} else if (currentPage + halfPagesToShow > totalPages)
		{
			setStartPage(totalPages - getButtonsToShow() + 1);
			setEndPage(totalPages);
			this.showBefore = true;
			this.showAfter = false;
		} else
		{
			setStartPage(currentPage - halfPagesToShow);
			setEndPage(currentPage + halfPagesToShow);
			this.showBefore = true;
			this.showAfter = true;
		}
	}

	public int getButtonsToShow()
	{
		return buttonsToShow;
	}

	public void setButtonsToShow(int buttonsToShow)
	{
		if (buttonsToShow % 2 != 0)
		{
			this.buttonsToShow = buttonsToShow;

		} else
		{
			throw new IllegalArgumentException("Must be an odd value!");
		}
	}

	public int getStartPage()
	{
		return startPage;
	}

	public void setStartPage(int startPage)
	{
		this.startPage = startPage;
	}

	public int getEndPage()
	{
		return endPage;
	}

	public int getTotalPages()
	{
		return totalPages;
	}

	public void setEndPage(int endPage)
	{
		this.endPage = endPage;
	}

	public int getCurrentPage()
	{
		return currentPage;
	}

	public boolean showPointsBefore()
	{
		return this.showBefore;
	}

	public boolean showPointsAfter()
	{
		return this.showAfter;
	}

	public List<Integer> getPageSizes()
	{
		return pageSizes;
	}

	public void setPageSizes(List<Integer> pageSizes)
	{
		this.pageSizes = pageSizes;
	}

	@Override
	public String toString()
	{
		return "Pager [startPage=" + startPage + ", endPage=" + endPage + "]";
	}
}
