package me.kyuuk.syslog.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.stereotype.Service;

import me.kyuuk.syslog.server.manager.SysLogEventManager;

@Service
public class HostOverseer
{
	private final Map<String, Set<String>> appPerHost = new HashMap<>();

	public HostOverseer(final SysLogEventManager manager)
	{
		final List<String> hosts = manager.getHosts();
		for (final String host : hosts)
		{
			this.appPerHost.put(host, new TreeSet<>(manager.getAppsforHost(host)));
		}
	}

	public synchronized void addHost(final String host)
	{
		if (!isKnownHost(host))
		{
			this.appPerHost.put(host, new TreeSet<>());
		}
	}

	public synchronized void addApp(final String host, final String app)
	{
		final Set<String> apps = this.appPerHost.get(host);
		apps.add(app);
	}

	private boolean isKnownHost(final String host)
	{
		return this.appPerHost.containsKey(host);
	}

	public List<String> getHosts()
	{
		return new ArrayList<>(this.appPerHost.keySet());
	}

	public List<String> getAppsForHost(final String host)
	{
		final Set<String> apps = this.appPerHost.get(host);
		return apps == null ? Collections.emptyList() : new ArrayList<>(apps);
	}

	public List<String> getApps()
	{
		final Set<String> apps = new HashSet<>();
		for (final Entry<String, Set<String>> appsPerHost : this.appPerHost.entrySet())
		{
			apps.addAll(appsPerHost.getValue());
		}
		return new ArrayList<>(apps);
	}

	public void removeHost(final String host)
	{
		this.appPerHost.remove(host);
	}

	public void clearAll()
	{
		this.appPerHost.clear();
	}

}
