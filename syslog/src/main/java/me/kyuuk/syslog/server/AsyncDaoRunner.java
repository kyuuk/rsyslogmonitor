package me.kyuuk.syslog.server;

import java.util.function.Supplier;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class AsyncDaoRunner
{

	private AsyncDaoRunner()
	{
		// utility class
	}

	public static void run(final String method, final DaoCall call, final Supplier<String> errorParamsMessageSupplier)
	{
		final Runnable task = () -> {
			// repositionnement du nom du logger applicatif dans le nouveau thread
			try
			{
				call.run();
			} catch (final Exception e)
			{
				log.error("Erreur lors de l'appel ASYNC de la methode {} {}", method, errorParamsMessageSupplier.get(),
						e);
			}
		};
		new Thread(task, Thread.currentThread().getName() + "-" + method + "-async").start();
	}

	@FunctionalInterface
	public interface DaoCall
	{
		void run();
	}

}
