package me.kyuuk.syslog.rfc5424.bean;

import org.apache.commons.lang3.StringUtils;

import me.kyuuk.syslog.rfc5424.exception.UnknownSeverity;

public enum Severity
{
	EMERGENCY(0, "system is unusable"), ALERT(1, "action must be taken immediately"),
	CRITICAL(2, "critical conditions"), ERROR(3, "error conditions"), WARNING(4, "warning conditions"),
	NOTICE(5, "normal but significant condition"), INFO(6, "informational messages"), DEBUG(7, "debug-level messages");

	private final int key;
	private final String label;

	private Severity(int key, String label)
	{
		this.key = key;
		this.label = label;
	}

	static Severity fromId(Integer key) throws UnknownSeverity
	{
		if (key == null)
		{
			throw new UnknownSeverity(key);
		}
		for (Severity sev : Severity.values())
		{
			if (sev.getKey() == key.intValue())
			{
				return sev;
			}
		}
		throw new UnknownSeverity(key);
	}

	public static Severity fromName(String key) throws UnknownSeverity
	{
		if (key == null)
		{
			throw new UnknownSeverity(key);
		}
		for (Severity sev : Severity.values())
		{
			if (StringUtils.equalsIgnoreCase(sev.name(), key))
			{
				return sev;
			}
		}
		throw new UnknownSeverity(key);
	}

	public int getKey()
	{
		return key;
	}

	public String getLabel()
	{
		return label;
	}

}
