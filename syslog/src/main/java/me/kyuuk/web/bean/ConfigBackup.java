package me.kyuuk.web.bean;

import java.util.Map;

public class ConfigBackup
{
	private String appVersion;
	private Map<String, Object> config;

	public String getAppVersion()
	{
		return this.appVersion;
	}

	public void setAppVersion(final String appVersion)
	{
		this.appVersion = appVersion;
	}

	public Map<String, Object> getConfig()
	{
		return this.config;
	}

	public void setConfig(final Map<String, Object> config)
	{
		this.config = config;
	}

}
