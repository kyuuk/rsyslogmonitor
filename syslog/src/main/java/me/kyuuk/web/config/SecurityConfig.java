package me.kyuuk.web.config;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import lombok.extern.log4j.Log4j2;

@EnableWebSecurity
@Log4j2
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
	private static final String PASSWORD_FIELD = "security.user.password";
	private static final String REMEMBER_ME_COOKIE_NAME = "syslogmon_rmbrme";

	@Value("${security.enabled:true}")
	private boolean isSecurityEnabled;
	@Value("${security.rememberme.tokenkey:}")
	private String tokenKey;

	@Override
	protected void configure(final HttpSecurity http) throws Exception
	{
		if (this.isSecurityEnabled)
		{
			http.authorizeRequests()
					.antMatchers("/api/**", "/css/**", "/js/**", "/img/**", "/sb-admin/**", "/", "/login-page*")
					.permitAll().anyRequest().authenticated().and().csrf().ignoringAntMatchers("/logout", "/api/**")
					.and().formLogin().loginPage("/login-page").permitAll().usernameParameter("jsusername")
					.passwordParameter("jspassword").defaultSuccessUrl("/index").permitAll().and().rememberMe()
					.rememberMeParameter("jsrememberme").rememberMeCookieName(REMEMBER_ME_COOKIE_NAME).and().logout()
					.logoutUrl("/logout").logoutSuccessUrl("/login-page?error=logout");
			if (StringUtils.isNotBlank(this.tokenKey))
			{
				log.info("adding remember-me key");
				log.debug(this.tokenKey);
				http.rememberMe().key(this.tokenKey);
			}
		} else
		{
			log.info("Web security is disabled ");
			http.authorizeRequests().anyRequest().permitAll().and().csrf().ignoringAntMatchers("/**");
		}
	}

	@Autowired
	public void configureGlobal(final AuthenticationManagerBuilder auth,
			@Value("${security.user.login:user}") final String username,
			@Value("${" + PASSWORD_FIELD + ":password}") final String password) throws Exception
	{
		auth.inMemoryAuthentication().passwordEncoder(NoOpPasswordEncoder.getInstance()).withUser(username)
				.password(password).roles("USER");
	}

	private static String encodePasswordIfNot(final String password, final String configFile)
	{
		log.info("Checking if password in properties [{}] is encoded", configFile);
		if (!StringUtils.startsWith(password, "{"))
		{
			log.debug("password is not encoded!");
			final PasswordEncoder encoder = new BCryptPasswordEncoder();
			final String encodedPassword = "{bcrypt}" + encoder.encode(password);
			try
			{
				final OutputStream output = new FileOutputStream(configFile);
				final Properties props = new Properties();
				props.setProperty(PASSWORD_FIELD, encodedPassword);
				props.store(output, null);
			} catch (final IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return encodedPassword;
		}
		return password;
	}

}
