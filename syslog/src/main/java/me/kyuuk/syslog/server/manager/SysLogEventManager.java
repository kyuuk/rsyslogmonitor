package me.kyuuk.syslog.server.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.log.audit.Audit;
import me.kyuuk.log.audit.AuditManager;
import me.kyuuk.syslog.exception.InvalidParamException;
import me.kyuuk.syslog.exception.InvalidSysLogEvent;
import me.kyuuk.syslog.rfc5424.bean.Severity;
import me.kyuuk.syslog.rfc5424.bean.SysLogEvent;
import me.kyuuk.syslog.rfc5424.exception.UnknownSeverity;
import me.kyuuk.syslog.server.manager.bean.LogCount;
import me.kyuuk.syslog.server.manager.bean.LogCountByApp;
import me.kyuuk.syslog.server.manager.validator.ManagerValidator;
import me.kyuuk.syslog.server.respository.AsyncLogEventRepository;
import me.kyuuk.syslog.server.respository.LogEventRepository;

@Component
@Log4j2
public class SysLogEventManager
{

	private final LogEventRepository repo;
	private final AsyncLogEventRepository asyncRepo;

	private static final Sort SORT_ORDER = Sort.by("timeStamp").descending();

	@Autowired
	public SysLogEventManager(final LogEventRepository repo, final AsyncLogEventRepository asyncRepo)
	{
		this.repo = repo;
		this.asyncRepo = asyncRepo;
	}

	public Long insertEvent(final SysLogEvent event) throws InvalidSysLogEvent
	{
		final Audit audit = new AuditManager("insertEvent");
		try
		{
			ManagerValidator.event(event);
			final SysLogEvent inserted = this.repo.save(event);
			return inserted.getId();
		} finally
		{
			audit.log();
		}

	}

	public List<SysLogEvent> getAll()
	{
		final Audit audit = new AuditManager("getAll");
		try
		{
			final List<SysLogEvent> events = IterableUtils.toList(this.repo.findAll());
			Collections.sort(events);
			return events;
		} finally
		{
			audit.log();
		}
	}

	public long getPageNumber(final int limit) throws InvalidParamException
	{
		final Audit audit = new AuditManager("getAll");
		try
		{
			ManagerValidator.limit(limit);
			final long count = this.repo.count();
			long nbrPages = count / limit;
			if (count % limit != 0)
			{
				nbrPages++;
			}
			return nbrPages;
		} finally
		{
			audit.log();
		}
	}

	public Page<SysLogEvent> getAllPaged(final int pageNbr, final int size, final String level, final String appName)
			throws InvalidParamException
	{
		final Audit audit = new AuditManager("getAllPaged");
		try
		{
			ManagerValidator.startLimits(pageNbr, size);
			final Pageable page = PageRequest.of(pageNbr, size, SORT_ORDER);

			if (StringUtils.isNotBlank(level) && StringUtils.isNotBlank(appName))
			{
				final Severity severity = Severity.fromName(level);
				return this.repo.findBySeverityAppName(severity, appName, page);
			} else if (StringUtils.isNotBlank(level))
			{
				log.debug("filtering on level");
				final Severity severity = Severity.fromName(level);
				return this.repo.findBySeverity(severity, page);
			} else if (StringUtils.isNotBlank(appName))
			{
				log.debug("filtering on appName");
				return this.repo.findByAppName(appName, page);
			}
			log.debug("no filter param getting all");
			return this.repo.findAll(page);
		} catch (final UnknownSeverity e)
		{
			throw new InvalidParamException("Severity", level);
		} finally
		{
			audit.log();
		}
	}

	public List<SysLogEvent> getByHost(final String host) throws InvalidParamException
	{
		final Audit audit = new AuditManager("getByHost");
		try
		{
			ManagerValidator.host(host);
			return this.repo.findByHost(host);
		} finally
		{
			audit.log();
		}
	}

	public Page<SysLogEvent> getByHostPaged(final String host, final int size, final int pageNbr, final String level,
			final String appName) throws InvalidParamException
	{
		log.info("start of getByHostPaged with params host=[{}], size=[{}], pageNbr=[{}], level=[{}], appName=[{}]",
				host, size, pageNbr, level, appName);
		final Audit audit = new AuditManager("getByHostPaged");
		try
		{
			ManagerValidator.host(host);
			final Pageable page = PageRequest.of(pageNbr, size, SORT_ORDER);
			if (StringUtils.isNotBlank(level) && StringUtils.isNotBlank(appName))
			{
				final Severity severity = Severity.fromName(level);
				return this.repo.findByHostSeverityApp(host, severity, appName, page);
			} else if (StringUtils.isNotBlank(level))
			{
				log.debug("filtering on level");
				final Severity severity = Severity.fromName(level);
				return this.repo.findByHostSeverity(host, severity, page);
			} else if (StringUtils.isNotBlank(appName))
			{
				log.debug("filtering on appName");
				return this.repo.findByHostAppName(host, appName, page);
			}
			log.debug("no filter param getting all");
			return this.repo.findByHost(host, page);
		} catch (final UnknownSeverity e)
		{
			throw new InvalidParamException("Severity", level);
		} finally
		{
			audit.log();
		}
	}

	public List<String> getHosts()
	{
		final Audit audit = new AuditManager("getHosts");
		try
		{
			return this.repo.getHosts();
		} finally
		{
			audit.log();
		}
	}

	public void clearAll()
	{
		final Audit audit = new AuditManager("clearAll");
		try
		{
			this.asyncRepo.clearAll();
		} finally
		{
			audit.log();
		}
	}

	public void clearByHost(final String host) throws InvalidParamException
	{
		final Audit audit = new AuditManager("clearByHost");
		try
		{
			ManagerValidator.host(host);
			this.asyncRepo.clearByHost(host);
		} finally
		{
			audit.log();
		}
	}

	public List<String> getappNames()
	{
		final Audit audit = new AuditManager("getappNames");
		try
		{
			return this.repo.getApps();
		} finally
		{
			audit.log();
		}
	}

	public List<String> getAppsforHost(final String host)
	{
		final Audit audit = new AuditManager("getAppsforHost");
		try
		{
			return this.repo.getAppsforHost(host);
		} finally
		{
			audit.log();
		}
	}

	public LogCount getCountsForHost(final String host)
	{
		final Audit audit = new AuditManager("getCountsForHost");
		try
		{
			return getCountForHost(host);
		} finally
		{
			audit.log();
		}
	}

	public List<LogCount> getAllCounts()
	{
		final Audit audit = new AuditManager("getAllCounts");
		final List<LogCount> result = new ArrayList<>();
		try
		{
			final List<String> hosts = this.repo.getHosts();
			for (final String host : hosts)
			{
				result.add(getCountForHost(host));
			}
			return result;
		} finally
		{
			audit.log();
		}
	}

	public LogCountByApp getCountsForHostApp(final String host)
	{
		final Audit audit = new AuditManager("getCountsForHostApp");
		try
		{
			return getCountByAppHost(host);
		} finally
		{
			audit.log();
		}
	}

	private LogCount getCountForHost(final String host)
	{
		final LogCount count = new LogCount(host);
		for (final Severity level : Severity.values())
		{
			count.addCount(level, this.repo.countByHostAndSeverity(host, level));
		}
		return count;
	}

	private LogCountByApp getCountByAppHost(final String host)
	{
		final LogCountByApp count = new LogCountByApp(host);
		final List<String> apps = this.repo.getAppsforHost(host);
		for (final String app : apps)
		{
			count.addAppCount(app, this.repo.countByHostAndAppName(host, app));
		}
		return count;
	}
}
