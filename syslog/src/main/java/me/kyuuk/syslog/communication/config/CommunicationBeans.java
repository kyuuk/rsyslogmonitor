package me.kyuuk.syslog.communication.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import me.kyuuk.custom.UserConfigProvider;
import me.kyuuk.syslog.communication.service.CommunicationManager;
import me.kyuuk.syslog.communication.service.EmailSenderImpl;

@Configuration
public class CommunicationBeans
{

	@Bean
	public CommunicationManager communicationManager(UserConfigProvider config)
	{
		switch (config.getNotificationType())
		{
			// TODO add sms notification
			case "email":
			default:
				return new EmailSenderImpl(config);
		}

	}
}
