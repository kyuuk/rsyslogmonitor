package me.kyuuk.syslog.communication.config.provider.email;

import java.util.Properties;

public class HotmailProvider extends AbstractEmailProvider
{
	private static final Properties additionalProps = new Properties();

	static
	{
		additionalProps.put("mail.smtp.auth", true);
	}

	public HotmailProvider(String user, String password)
	{
		super("smtp-mail.outlook.com", 587, user, password, true, "smtp");
	}

	@Override
	public Properties getAdditionalProperties()
	{
		return additionalProps;
	}

}
