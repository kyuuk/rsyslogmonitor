package me.kyuuk.supervisor.advice;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.supervisor.bean.ExecutionState;
import me.kyuuk.supervisor.exceptions.ApplicationNotFound;
import me.kyuuk.supervisor.exceptions.ExecutorUnreachable;
import me.kyuuk.syslog.exception.InvalidParamException;
import me.kyuuk.web.ws.bean.ApiResponse;
import me.kyuuk.web.ws.bean.ApiResponseCodes;

/**
 * RestController exception handler
 *
 * @author kyuk
 *
 */
@RestControllerAdvice
@Log4j2
public class RestExceptionsHandler
{

	@ExceptionHandler(InvalidParamException.class)
	public ApiResponse<ExecutionState> invalidParamExceptionHandler(final InvalidParamException e)
	{
		log.warn(e);
		return new ApiResponse<>(ApiResponseCodes.INVALID_PARAM);
	}

	@ExceptionHandler(ApplicationNotFound.class)
	public ApiResponse<ExecutionState> applicationNotFoundExceptionHandler(final ApplicationNotFound e)
	{
		log.error(e);
		return new ApiResponse<>(ApiResponseCodes.APPLICATION_NOT_FOUND);
	}

	@ExceptionHandler(ExecutorUnreachable.class)
	public ApiResponse<ExecutionState> executorUnreachableExceptionHandler(final ExecutorUnreachable e)
	{
		log.error(e);
		return new ApiResponse<>(ApiResponseCodes.EXECUTOR_UNREACHABLE);
	}

}
