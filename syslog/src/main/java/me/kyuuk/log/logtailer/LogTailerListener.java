package me.kyuuk.log.logtailer;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class LogTailerListener implements TailerListener
{

	private String fileName;

	@Override
	public void init(final Tailer tailer)
	{
		this.fileName = tailer.getFile().getAbsolutePath();
		log.info("Tailer Listener init with file [{}]", this.fileName);
	}

	@Override
	public void fileNotFound()
	{
		log.error("File [{}] Not Found", this.fileName);
	}

	@Override
	public void fileRotated()
	{
		LogTailer.clearLogs();
	}

	@Override
	public void handle(final String line)
	{
		LogTailer.addLog(line);
	}

	@Override
	public void handle(final Exception ex)
	{
		log.error("Exception : ", ex);
	}
}
