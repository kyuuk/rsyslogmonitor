package me.kyuuk.syslog.server.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.syslog.RFC5424MessageConverter;
import org.springframework.integration.syslog.inbound.SyslogReceivingChannelAdapterSupport;
import org.springframework.integration.syslog.inbound.TcpSyslogReceivingChannelAdapter;
import org.springframework.integration.syslog.inbound.UdpSyslogReceivingChannelAdapter;

import lombok.extern.log4j.Log4j2;

@Configuration
@ConditionalOnProperty(prefix = "RsyslogServer", name = "enabled")
@Log4j2
public class SpringIntegrationConfig
{
	@Bean
	public SyslogEventHandler SyslogEventHadler()
	{
		return new SyslogEventHandler();
	}

	@Bean
	public IntegrationFlow processUniCastUdpMessage(@Value("${RsyslogServer.protocol}") final String protocol)
	{
		SyslogReceivingChannelAdapterSupport adapter;
		if (StringUtils.equalsIgnoreCase(protocol, "TCP"))
		{
			adapter = new TcpSyslogReceivingChannelAdapter();
		} else
		{
			adapter = new UdpSyslogReceivingChannelAdapter();
		}
		adapter.setConverter(new RFC5424MessageConverter());
		return IntegrationFlows.from(adapter).handle("SyslogEventHadler", "handleMessage").get();
	}
}
