package me.kyuuk.syslog;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

public class DateTest
{

	@Test
	public void testParseDate() throws Exception
	{
		String ts = "2019-05-18T21:02:37.331569+02:00";
		LocalDateTime date = LocalDateTime.parse(ts, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		assertEquals(2019, date.getYear());
		assertEquals(5, date.getMonthValue());
		assertEquals(18, date.getDayOfMonth());
		assertEquals(21, date.getHour());
		assertEquals(02, date.getMinute());
	}

}
