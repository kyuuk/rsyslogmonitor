package me.kyuuk.custom.properties;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.custom.CustomPropertySource;
import me.kyuuk.custom.exception.InitializationException;

@Log4j2
public class PropertiesCustomPropertySource implements CustomPropertySource
{
	private final File propertiesFile;
	private final Properties props;

	public PropertiesCustomPropertySource(final String fileName) throws InitializationException
	{
		this(new File(fileName));
	}

	public PropertiesCustomPropertySource(final File propertiesFile) throws InitializationException
	{
		this.propertiesFile = propertiesFile;
		this.props = new Properties();
		load();
	}

	@Override
	public void load() throws InitializationException
	{
		try (FileReader reader = new FileReader(this.propertiesFile))
		{
			this.props.load(reader);
		} catch (final IOException e)
		{
			throw new InitializationException(
					"error while loading peroperties from file [" + this.propertiesFile.getAbsolutePath() + "]", e);
		}
	}

	@Override
	public void store() throws IOException
	{
		try (final FileWriter writer = new FileWriter(this.propertiesFile))
		{
			this.props.store(writer, null);
	}
	}

	@Override
	public String getProperty(final String name)
	{
		return this.props.getProperty(name);
	}

	@Override
	public Properties getAllProperties()
	{
		return this.props;
	}

	@Override
	public Map<String, String> getAllPrefixed(final String prefix)
	{
		final Map<String, String> toReturn = new HashMap<>();
		for (final String key : this.props.stringPropertyNames())
		{
			if (StringUtils.startsWithIgnoreCase(key, prefix))
			{
				toReturn.put(key, (String) this.props.get(key));
			}
		}
		return toReturn;
	}

	@Override
	public String getPropertyOrDefault(final String name, final String defaultValue)
	{
		return this.props.getProperty(name, defaultValue);
	}

	@Override
	public void addProperty(final String name, final String value) throws IOException
	{
		this.props.put(name, value);
		store();
	}

	@Override
	public void removeProperty(final String name) throws IOException
	{
		this.props.remove(name);
		store();
	}

	@Override
	public void refresh()
	{
		this.props.clear();
		try
		{
			load();
		} catch (final InitializationException e)
		{
			log.error("Could not refresh config from [{}] using cache", this.propertiesFile.getAbsolutePath(), e);
		}
	}

	@Override
	public String toString()
	{
		return this.propertiesFile.getAbsolutePath();
	}

	public static Map<String, String> getAsMap(final Properties properties)
	{
		return properties.entrySet().stream()
				.collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue().toString()));
	}

	@Override
	public void addAll(final Properties props) throws IOException
	{
		this.props.putAll(props);
		store();
	}
}
