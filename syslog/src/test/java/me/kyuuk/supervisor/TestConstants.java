package me.kyuuk.supervisor;

public class TestConstants
{
	public static final String STATUS_LINE = "active (running) since Sat 2019-03-16 16:16:21 CET; 2 days ago";
	public static final String OUTPUT_ERROR = "ssh: error can not connect to host";
	public static final String OUTPUT = "nginx.service - A high performance web server and a reverse proxy server\r\n"
			+ "   Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)\r\n"
			+ "   Active: active (running) since Sat 2019-03-16 16:16:21 CET; 2 days ago\r\n"
			+ "     Docs: man:nginx(8)\r\n"
			+ "  Process: 3867 ExecStop=/sbin/start-stop-daemon --quiet --stop --retry QUIT/5 --pidfile /run/nginx.pid (code=exited, st\r\n"
			+ "  Process: 27225 ExecReload=/usr/sbin/nginx -g daemon on; master_process on; -s reload (code=exited, status=0/SUCCESS)\r\n"
			+ "  Process: 3873 ExecStart=/usr/sbin/nginx -g daemon on; master_process on; (code=exited, status=0/SUCCESS)\r\n"
			+ "  Process: 3870 ExecStartPre=/usr/sbin/nginx -t -q -g daemon on; master_process on; (code=exited, status=0/SUCCESS)\r\n"
			+ " Main PID: 3874 (nginx)\r\n" + "   CGroup: /system.slice/nginx.service\r\n"
			+ "           ├─ 3874 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;\r\n"
			+ "           ├─27228 nginx: worker process\r\n" + "           ├─27229 nginx: worker process\r\n"
			+ "           ├─27230 nginx: worker process\r\n" + "           └─27231 nginx: worker process\r\n" + "\r\n"
			+ "Mar 18 17:55:45 perso.kyuuk.me systemd[1]: Reloading A high performance web server and a reverse proxy server.\r\n"
			+ "Mar 18 17:55:45 perso.kyuuk.me systemd[1]: Reloaded A high performance web server and a reverse proxy server.\r\n"
			+ "Mar 18 18:00:03 perso.kyuuk.me systemd[1]: Reloading A high performance web server and a reverse proxy server.\r\n"
			+ "Mar 18 18:00:03 perso.kyuuk.me systemd[1]: Reloaded A high performance web server and a reverse proxy server.\r\n"
			+ "Mar 18 18:01:29 perso.kyuuk.me systemd[1]: Reloading A high performance web server and a reverse proxy server.\r\n"
			+ "Mar 18 18:01:29 perso.kyuuk.me systemd[1]: Reloaded A high performance web server and a reverse proxy server.\r\n"
			+ "Mar 19 10:44:37 perso.kyuuk.me systemd[1]: Reloading A high performance web server and a reverse proxy server.\r\n"
			+ "Mar 19 10:44:37 perso.kyuuk.me systemd[1]: Reloaded A high performance web server and a reverse proxy server.\r\n"
			+ "Mar 19 10:51:08 perso.kyuuk.me systemd[1]: Reloading A high performance web server and a reverse proxy server.";
}
