package me.kyuuk.web.ws.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NonNull;

@Data
public class ApiResponse<T>
{
	@JsonIgnore
	@NonNull
	private final ApiResponseCodes responseCode;
	private T response;

	public ApiResponse(final ApiResponseCodes responseCode)
	{
		this(responseCode, null);
	}

	public ApiResponse(final ApiResponseCodes responseCode, final T response)
	{
		this.responseCode = responseCode;
		this.response = response;
	}

	@JsonProperty("errorCode")
	public int getCode()
	{
		return this.responseCode.getCode();
	}

	@JsonProperty("message")
	public String getMessage()
	{
		return this.responseCode.getMessage();
	}
}
