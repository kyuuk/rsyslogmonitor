package me.kyuuk.crypto;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CryptoCommons
{
	public static final String PROVIDER_NAME = BouncyCastleProvider.PROVIDER_NAME;

	private CryptoCommons()
	{
		// utility class
	}

	public static void addBouncyCastleProvider()
	{
		// Gestion de l'enregistrement de Bouncy Castle en tant que provider de crypto
		if (Security.getProvider(PROVIDER_NAME) == null)
		{
			Security.addProvider(new BouncyCastleProvider());
		}
	}

	/**
	 * Transforme une String en tableau de bytes en utilisant l'encodage spécifié.
	 * Si l'encoding n'existe pas, nous utilisons l'encodage par défaut
	 *
	 * @param string   String à transformer
	 * @param encoding encode à utiliser (obligatoire)
	 * @return tableau de bytes
	 */
	public static byte[] getBytes(final String string, final String encoding)
	{
		if (string == null)
		{
			return new byte[0];
		}

		byte[] result = null;
		try
		{
			result = string.getBytes(encoding);
		} catch (final UnsupportedEncodingException e)
		{
			log.fatal("Encoding [{}] non supporte par le serveur, on bascule sur l'encodage par defaut : ", encoding,
					e);
			result = string.getBytes(StandardCharsets.UTF_8);
		}
		return result;
	}
}
