function submitform(formId) {
	$(formId).submit();
}

function getMeta(dataVar) {
	return $('meta[data-var="' + dataVar + '"]').data('content');
}

const webRoot = getMeta('root');

const RSYSLOGMONITOR = {
	common: {
		init() {
			// nothing
		}
	},
	dashboard: {
		init() {
			function fillLogStatChart(canvas, data) {
				var ctx = canvas.getContext("2d");
				var labels = [];
				var datasets = [
					{ backgroundColor: "green", 'data': [], label: "Others" },
					{ backgroundColor: "red", 'data': [], label: "Errors" }
				];
				data.response.forEach((entry) => {
					labels.push(entry.host);
					datasets[0].data.push(entry.total - entry.errors);
					datasets[1].data.push(entry.errors);
				});
				var myChart = new Chart(ctx, {
					type: "bar",
					data: {
						'labels': labels,
						'datasets': datasets
					},

					options: {
						responsive: true,
						maintainAspectRatio: false,
						scales: {
							xAxes: [{ stacked: true, barPercentage: 0.4 }],
							yAxes: [{ stacked: true }],
						},
						tooltips: {
							mode: "label",
							callbacks: {
								label: function(t, d) {
									var dstLabel = d.datasets[t.datasetIndex].label;
									var yLabel = t.yLabel;
									return dstLabel + ": " + yLabel;
								}
							}
						}
					}
				});
				canvas.onclick = function(e) {
					var slice = myChart.getElementAtEvent(e);
					if (!slice.length) return; // return if not clicked on slice
					var label = slice[0]._model.label;
					window.location.href = webRoot + "/hostStats?host=" + label;
				}
			}
			var logStatsCanvas = document.getElementById("logStats");

			const url = webRoot + '/api/getLogStats';
			$.get(url, data => {
				fillLogStatChart(logStatsCanvas, data);
			});
		}
	},
	status: {
		init() {
			$(".refreshable").on('click', function() {
				var row = $(this).parent().parent();
				RSYSLOGMONITOR.status.refresh(row);
			});
			$("#AutoRefresh").change(function() {
				if (this.checked) {
					RSYSLOGMONITOR.status.refreshAll();
				}
			});
		},
		refresh(row) {
			var appName = row.find("td:eq(0)").text().trim();
			var host = row.find("td:eq(1)").text().trim();
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");
			var headers = {};
			headers[header] = token;
			var settings = {
				headers: headers,
				url: '/logmonitor/api/refresh',
				data: {
					'appName': appName,
					'host': host
				},
				dataType: 'json'
			};
			$.post(settings).done(function(data) {
				RSYSLOGMONITOR.status.replace(row, data);
			});
		},
		replace(row, data) {
			// exitCode
			row.find("td:eq(2)").text(data.response.exitCode);
			if (data.response.exitCode == 0) {
				row.attr("class", "table-success");
			} else if (data.response.exitCode != 0) {
				row.attr("class", "table-danger");
			}

			// status || error
			if (data.response.status == "" || data.response.status == null) {
				row.find("td:eq(3)").text(data.response.outputErr);
			} else {
				row.find("td:eq(3)").text(data.response.status);
			}
		},
		refreshAll() {
			if ($("#AutoRefresh").is(":checked")) {
				var timeout = 0;
				$('#Table > tbody  > tr').each(function() {
					timeout = timeout + 5000;
					setTimeout(refresh, timeout, $(this));
				});
				timeout = timeout + 10000;
				setTimeout(refreshAll, timeout);
			}
		}
	},
	appLogs: {
		init() {
			$('#log_update_toggle').on('click', function() {
				const wasActive = $(this).data('state') === 'active'; // State before clicking
				$(this).data('state', wasActive ? 'paused' : 'active');
				$(this).find('i').toggleClass('fa-pause fa-play');
				$(this).find('span').text(wasActive ? 'Resume' : 'Pause');
				$(this).attr('title', wasActive ? 'Resume updating the log on this page.' : 'Pause updating the log on this page.');
				return false;
			});
			RSYSLOGMONITOR.appLogs.updateLogData();
		},
		updateLogData() {
			if ($('#log_update_toggle').data('state') === 'active') {
				const postData = 'min_level=' + $('select[name=min_level]').val() + '&log_filter=' + $('select[name=log_filter]').val() + '&log_search=' + $('#log_search').val();
				const url = webRoot + '/applicationLog';
				$.get(url, postData, data => {
					$('pre').html($(data).find('pre').html());
				});
			}
			setTimeout(RSYSLOGMONITOR.appLogs.updateLogData, 2000);
		}
	},
	hostStats: {
		init() {
			function buildChart(ctx, chartType, datasets, labels) {
				return new Chart(ctx, {
					type: chartType,
					data: {
						'labels': labels,
						'datasets': datasets
					}
				});
			}
			var dynamicColors = function() {
				var r = Math.floor(Math.random() * 255);
				var g = Math.floor(Math.random() * 255);
				var b = Math.floor(Math.random() * 255);
				return "rgb(" + r + "," + g + "," + b + ")";
			};
			var logStatsCanvas = document.getElementById("byApp");
			const urlByApp = webRoot + '/api/getAppStatsForHost';
			$.post(urlByApp, "host=" + host, result => {
				var labels = [];
				var datasets = [
					{ 'backgroundColor': [], 'data': [], 'label': "Others" }
				];
				for (var entry in result.response.countByApp) {
					labels.push(entry);
					datasets[0].data.push(result.response.countByApp[entry]);
					datasets[0].backgroundColor.push(dynamicColors());
				}
				buildChart(logStatsCanvas, "doughnut", datasets, labels);
			});
			//TODO add chart for logs by level stats
			var byLevelStatsCanvas = document.getElementById("byLevel");
			const urlBylevel = webRoot + '/api/getlevelStatsForHost';
			$.post(urlBylevel, "host=" + host, result => {
				var labels = [];
				var datasets = [
					{ 'backgroundColor': [], 'data': [], 'label': "Others" }
				];
				for (var entry in result.response.counts) {
					labels.push(entry);
					datasets[0].data.push(result.response.counts[entry]);
					datasets[0].backgroundColor.push(dynamicColors());
				}
				buildChart(byLevelStatsCanvas, "doughnut", datasets, labels);
			});
		}
	}

}

const UTIL = {
	exec(controller, action) {
		const nameSpace = RSYSLOGMONITOR;
		action = (action === undefined) ? 'init' : action;

		if (controller !== '' && nameSpace[controller] && typeof nameSpace[controller][action] === 'function') {
			nameSpace[controller][action]();
		}
	},
	init() {
		const body = document.body;
		const controller = body.getAttribute('data-controller');
		const action = body.getAttribute('data-action');

		UTIL.exec('common');
		UTIL.exec(controller);
		UTIL.exec(controller, action);
	}
};

$(document).ready(UTIL.init);