package me.kyuuk.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import me.kyuuk.web.bean.CssClassProcessor;

@Configuration
public class CustomBeans
{

	@Bean
	public CssClassProcessor cssClassProcessor()
	{
		return new CssClassProcessor();
	}
}
