package me.kyuuk.supervisor.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.supervisor.bean.Application;
import me.kyuuk.supervisor.bean.ExecutionState;
import me.kyuuk.supervisor.exceptions.ApplicationNotFound;
import me.kyuuk.supervisor.exceptions.ExecutorUnreachable;
import me.kyuuk.syslog.exception.InvalidParamException;

@Component
@EnableScheduling
@Log4j2
public class AppChecker
{

	private final ExecutorClientService executor;
	private Map<Application, ExecutionState> stats = new HashMap<>();

	public AppChecker(final ExecutorClientService executor)
	{
		this.executor = executor;
	}

	@Scheduled(fixedDelayString = "${supervisor.refreshDelay}", initialDelay = 5000)
	public void refreshAll()
	{
		log.info("Starting automatic refresh of all applications status");
		try
		{
			this.setStats(this.executor.getAllStatus());
		} catch (final ExecutorUnreachable e)
		{
			log.error("could not refresh services status [{}]", e.getMessage());
		}
	}

	public ExecutionState forceRefresh(final String appName, final String host)
			throws InvalidParamException, ApplicationNotFound, ExecutorUnreachable
	{
		final Application app = new Application(appName, host);
		final ExecutionState status = this.executor.getStatus(appName, host);
		this.refreshApp(app, status);
		return status;
	}

	public Map<Application, ExecutionState> getStats()
	{
		return this.stats;
	}

	public void setStats(final Map<Application, ExecutionState> stats)
	{
		this.stats = stats;
	}

	public void refreshApp(final Application app, final ExecutionState status)
	{
		this.stats.put(app, status);
	}

	public ExecutionState getAppStatus(final String appName, final String host)
	{
		final Application app = new Application(appName, host);
		return getStats().get(app);
	}

	public Map<Application, ExecutionState> getAllStatus()
	{
		return getStats();
	}

	public Map<Application, String> getSimpleStatus()
	{
		final Map<Application, String> simpleStatus = new HashMap<>();
		for (final Entry<Application, ExecutionState> entry : getStats().entrySet())
		{
			simpleStatus.put(entry.getKey(), entry.getValue().getExitCode() == 0 ? "OK" : "Fail");
		}
		return simpleStatus;
	}
}
