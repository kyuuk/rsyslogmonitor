package me.kyuuk.custom;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;
import me.kyuuk.crypto.Cipherer;
import me.kyuuk.custom.properties.PropertiesCustomPropertySource;
import me.kyuuk.syslog.communication.config.provider.email.AbstractEmailProvider;
import me.kyuuk.syslog.communication.config.provider.email.CustomEmailProvider;
import me.kyuuk.syslog.communication.config.provider.email.GmailProvider;
import me.kyuuk.syslog.communication.config.provider.email.HotmailProvider;

@Component
@EnableScheduling
@Log4j2
public class UserConfigProvider
{
	private static final List<String> propertiesToCipher = new ArrayList<>();

	static
	{
		propertiesToCipher.add("user.mail.sender.password");
	}
	private final CustomPropertySource propertySource;
	private final Cipherer cipherer;
	private final ApplicationContext context;

	private final Environment env;

	public UserConfigProvider(@Value("${userConfig.file:user.config}") final String propertyFileName,
			final ApplicationContext context, final Environment env)
	{
		log.info("initializing userConfig from File [{}]", propertyFileName);
		this.context = context;
		this.env = env;
		this.propertySource = new PropertiesCustomPropertySource(propertyFileName);
		this.cipherer = new Cipherer(getOrDefault("cipherKey", "DefaultCipherKey"));
	}

	@Scheduled(fixedDelayString = "${userconfig.refreshTime:‭1800000‬}") // default refresh delay 30Min
	public void refresh()
	{
		log.info("refreshing properties from [{}]", this.propertySource);
		this.propertySource.refresh();
		log.info("refreshing Spring context");
		this.context.getBeansOfType(RefreshableBean.class).forEach((k, v) -> v.refresh());
	}

	public Map<String, Object> getAllPropertiesAsMap()
	{
		return getAsMap(this.propertySource.getAllProperties());
	}

	public Map<String, String> getWithPrefix(final String prefix)
	{
		return this.propertySource.getAllPrefixed(prefix);
	}

	public String getProperty(final String key)
	{
		String propValue = this.propertySource.getProperty(key);
		if (StringUtils.isNotBlank(propValue))
		{
			try
			{
				if (propertiesToCipher.contains(key))
				{
					if (this.cipherer.isCiphered(propValue))
					{
						propValue = this.cipherer.decipherString(propValue);
					} else
					{
						saveProperty(key, cipherProperty(key, propValue));
					}
				}
			} catch (final IOException e)
			{
				log.warn("property ciphering failed with cause [{}], will be tried next time", e.getMessage());
			}
		} else
		{
			propValue = this.env.getProperty(key);
		}
		return propValue != null ? propValue : "";
	}

	public int getIntProperty(final String key)
	{
		final String strValue = getProperty(key);
		if (StringUtils.isNumeric(strValue))
		{
			return NumberUtils.createInteger(strValue).intValue();
		}
		return -1;
	}

	public void saveProperty(final String key, final String value) throws IOException
	{
		log.info("Saving property [{}] with value [{}]", key, value);
		this.propertySource.addProperty(key, cipherProperty(key, value));
	}

	public void saveAllProperties(final Properties props) throws IOException
	{
		log.info("Saving bulk properties");
		this.propertySource.addAll(props);
	}

	public void removeProperty(final String key) throws IOException
	{
		this.propertySource.removeProperty(key);
	}

	public List<String> getListProperty(final String key)
	{
		final String strList = getProperty(key);
		if (StringUtils.isBlank(strList))
		{
			return new ArrayList<>();
		}
		return Arrays.stream(StringUtils.split(strList, ",")).map(StringUtils::trim).collect(Collectors.toList());
	}

	public JavaMailSenderImpl getMailSender()
	{
		final String mailprovider = getProperty("mailProvider");
		AbstractEmailProvider provider;
		final String username = getProperty("user.mail.sender.username");
		final String password = getProperty("user.mail.sender.password");
		switch (mailprovider)
		{
			case "Gmail":
				provider = new GmailProvider(username, password);
				break;
			case "Live":
				provider = new HotmailProvider(username, password);
				break;

			default:
				provider = new CustomEmailProvider(this);
		}
		return provider.getMailSender();
	}

	public String getNotificationType()
	{
		return getProperty("logMonitor.notification.message.type");
	}

	public String getOrDefault(final String key, final String defaultValue)
	{
		final String value = getProperty(key);
		return StringUtils.isNotBlank(value) ? value : defaultValue;
	}

	public boolean getBoolanProperty(final String key)
	{
		return BooleanUtils.toBoolean(getProperty(key));
	}

	private String cipherProperty(final String key, final String value)
	{
		log.debug("Checking if property [{}] need to be ciphered", key);
		try
		{
			if (propertiesToCipher.contains(key) && !this.cipherer.isCiphered(value))
			{
				log.info("Chiphering property [{}] before saving", key);
				return this.cipherer.cipherString(value);
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | NoSuchProviderException e)
		{
			log.warn("Could not cipher property [{}] before saving. Writing clear value", key, e);
		}
		return value;
	}

	private static Map<String, Object> getAsMap(final Properties properties)
	{
		return properties.entrySet().stream().collect(
				Collectors.toMap(e -> String.valueOf(e.getKey()), Entry::getValue, (prev, next) -> next, HashMap::new));
	}
}
